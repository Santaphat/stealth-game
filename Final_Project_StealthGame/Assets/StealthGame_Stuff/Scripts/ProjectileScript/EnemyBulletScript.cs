﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletScript : MonoBehaviour
{
    private void Start()
    {
        Destroy(this.gameObject, 3);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.tag == "Player")
        {
            collision.gameObject.GetComponent<PlayerProperty>().HitPlayer();
            Destroy(this.gameObject);
        }
        else if(collision.transform.tag == "GameEnvironment")
        {
            Destroy(this.gameObject);
        }
    }
}
