﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class EnemyFSM : MonoBehaviour
{
 
    [SerializeField]
    FSMStateType _startingState;

    [SerializeField]
    List<BaseState> _validStates;
    BaseState _currentState;
    BaseState _previousState;

    BaseEnemy baseEnemy;

    Dictionary<FSMStateType, BaseState> _FSMState;
     
    private void Awake()
    {
        _currentState = null;

        _FSMState = new Dictionary<FSMStateType, BaseState>();

        NavMeshAgent navMeshAgent = this.GetComponent<NavMeshAgent>();
        baseEnemy = this.GetComponent<BaseEnemy>();
        Animator AC = this.GetComponent<Animator>();

        foreach (BaseState state in _validStates)
        {
            if (state != null)
            {
                var temp = Instantiate(state);
                temp.SetExecutingFSM(this);
                temp.SetExecutingEnemy(baseEnemy);
                if (baseEnemy.GetIsHumaniod())
                {
                    temp.SetHumaniodBase((HumaniodEnemy)baseEnemy);
                    temp.SetNavMeshAgent(navMeshAgent);
                    temp.SetExecutingAnimator(AC);
                }
                _FSMState.Add(temp.StateType, temp);
            }
        }

    }
    private void Start()
    {
        if(_startingState != null)
        {
            EnterState(_startingState);
        }
    }

    private void Update()
    {
        if(_currentState != null)
        {
            _currentState.UpdateState();
        }
    }
    
    public FSMStateType CurrentStateType
    {
        get{
            return _currentState.StateType;
        }
    }

    #region StateManagemnt

    public void EnterState(BaseState nextState)
    {
        if (baseEnemy.CurrentState != baseEnemyState.Dead)
        {
            if (nextState == null)
            {
                return;
            }
            if (_currentState != null)
            {
                _currentState.ExitState();
            }
            _previousState = _currentState;
            _currentState = nextState;
            _currentState.EnterState();
        }
    }

    public void EnterState(FSMStateType stateType)
    {
        if (baseEnemy.CurrentState != baseEnemyState.Dead)
        {
            if (_FSMState.ContainsKey(stateType))
            {
                BaseState nextState = _FSMState[stateType];

                EnterState(nextState);
            }
            else
            {
                Debug.LogError("Cannot Find " + stateType + " Inside FSM");
            }
        }
    }

    public void ExitState()
    {

    }
    #endregion
}
