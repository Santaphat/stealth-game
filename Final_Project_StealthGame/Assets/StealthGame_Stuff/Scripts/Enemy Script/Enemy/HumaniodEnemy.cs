﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum NoneAlertSubState
{
    Patrol,
    Guard,
}

//Humaniod Enemy class is a base class for those enemy that 
//Can hear, move and has a head and has animation

[RequireComponent(typeof(NavMeshAgent), typeof(Animator))]
abstract public class HumaniodEnemy : BaseEnemy
{
    [SerializeField]
    protected float _hearDistance;

    [SerializeField]
    protected Transform _headTransform;

    [SerializeField]
    protected bool _patrolLoop;

    [SerializeField]
    protected PatrolMarker[] _patrolPoints;

    [SerializeField]
    protected NoneAlertSubState _humaniodNonAlertSubState = NoneAlertSubState.Guard;

    protected NavMeshAgent _navMeshAgent;

    protected Animator _AC;

    protected Vector3 _HeardSomethingPosition;

    protected new bool _isHumaniod = true;

    protected override void Awake()
    {
        isHumaniod = true;
        base.Awake();
        _navMeshAgent = this.GetComponent<NavMeshAgent>();
        _AC = this.GetComponent<Animator>();
    }
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }
    protected override void OnDrawGizmos()
    {
        base.OnDrawGizmos();
        Ray test = new Ray(_headTransform.position, _headTransform.forward);
        Gizmos.color = Color.red;
        Gizmos.DrawRay(test);
        if (_navMeshAgent != null)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawLine(this.transform.position, _navMeshAgent.destination);
        }
    }

    public override void enemyGotHit()
    {
        base.enemyGotHit();
        _AC.SetBool("isAlive", false);
    }


    public void EnemyHeardSomething(Vector3 objectPostion)
    {
        //if (GameUtilities.CalculatePathLength(_navMeshAgent, objectPostion) < _hearDistance)
            if (_finiteStateMachine.CurrentStateType != FSMStateType.COMBAT && _finiteStateMachine.CurrentStateType != FSMStateType.DetectingPlayer)
            {
                //Debug.Log(this.gameObject.name + "HeardPos: " + objectPostion + " Distance:" + Vector3.Distance(objectPostion, this.transform.position));
                HeardSomethingPosition = objectPostion;
            if (_finiteStateMachine.CurrentStateType != FSMStateType.HeardSomethingState)
            {
                _finiteStateMachine.EnterState(FSMStateType.HeardSomethingState);
            }
            }
        //}
    }

    public void OtherEnemyDetectedPlayer(Vector3 objectPostion)
    {
        //Debug.Log(this.name + "PathLength: " + GameUtilities.CalculatePathLength(_navMeshAgent, objectPostion));
        //if (GameUtilities.CalculatePathLength(_navMeshAgent, objectPostion) < _hearDistance)
 
            SetDestination(objectPostion);
            _playerLastKnownLocation = objectPostion;
            if (_finiteStateMachine.CurrentStateType != FSMStateType.COMBAT)
            {
                _finiteStateMachine.EnterState(FSMStateType.COMBAT);
            }
        //}
    }

    public override bool GetIsHumaniod()
    {
        return true;
    }
    public virtual void StopMovement()
    {
        _AC.SetFloat("Speed", 0);
        _navMeshAgent.speed = 0;
        _navMeshAgent.isStopped = true;
    }

    public virtual void StartMovement(float moveSpeed)
    {
        _AC.SetFloat("Speed", moveSpeed);
        _navMeshAgent.speed = moveSpeed;
        _navMeshAgent.isStopped = false;
    }

    public override bool canSeeObject(Transform _targetObject, string targetedTag, bool ignoreTrigger)
    {
        Vector3 direction = _targetObject.position - _headTransform.position;
        direction.y = 0;
        float angle = Vector3.Angle(direction, this.transform.forward);
        //Debug.Log(angle);
        if (angle < _enemyFov * 0.5f)
        {
            Debug.DrawLine(_headTransform.position, _targetObject.position);
            RaycastHit hit;
            Vector3 newPos = _headTransform.position;
            newPos.y = +0.5f;
            QueryTriggerInteraction ignore = QueryTriggerInteraction.Collide;
            if (ignoreTrigger)
            {
                ignore = QueryTriggerInteraction.Ignore;
            }

            if (Physics.Raycast(newPos, direction, out hit, _sightDistance, _obstacleMaskRayCast, ignore))
            {
                if (hit.collider != null)
                {
                    //Debug.Log(hit.collider.gameObject);
                    if (hit.collider.tag == targetedTag)
                    { 
                        //Debug.Log("Hit");
                        return true;
                    }
                }
            }
        }
        return false;
    }
    void OnAnimatorIK()
    {
        //Debug.Log("Enter");
        if (_AC != null)
        {

            //Debug.Log("Enter");
            if (_lookatObjectOfInterest != Vector3.zero)
            {

                //Debug.Log("Enter Inside");
                _AC.SetLookAtWeight(1, 0, 0.5f, 0.5f, 0.7f);
                _AC.SetLookAtPosition(_lookatObjectOfInterest);
            }
            else
            {
                _AC.SetLookAtWeight(0);
            }

        }
    }

    public void SetDestination(Vector3 destination)
    {
        if (_navMeshAgent != null && destination != null)
        {
            _navMeshAgent.SetDestination(destination);
        }
    }

    public bool PatrolLoop
    {
        get
        {
            return _patrolLoop;
        }
    }

    public Vector3 HeardSomethingPosition
    {
        get
        {
            return _HeardSomethingPosition;
        }
        set
        {
            _HeardSomethingPosition = value;
        }
    }
    public PatrolMarker[] PatrolPoints
    {
        get
        {
            return _patrolPoints;
        }
    }

    public NoneAlertSubState HumaniodNonAlertSubState
    {
        get
        {
            return _humaniodNonAlertSubState;
        }
    }
}
