﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeGuard : NormalGuard
{
    [SerializeField]
    float _attackDistance;
    protected override void Awake()
    {
        base.Awake();
        _enemyType = EnemyType.MeleeGuard;
    }
    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }
    public override bool Attacking()
    {
        //Debug.Log("Enter Attack");
        _isAttacking = true;
        bool attackSuccessful = false;
        _sem.PlaySoundEffect("Attack");
        _AC.SetTrigger("Attack");
        return attackSuccessful;
    }

    bool checkIfObjectIsNear(GameObject targetObject)
    {
        if (targetObject != null)
        {
            float cosAngle = Vector3.Dot(
           (targetObject.transform.position - this.transform.position).normalized,
           this.transform.forward);

            float angle = Mathf.Acos(cosAngle) * Mathf.Rad2Deg;
            //Debug.Log(angle);
            if (angle < 90)
            {
                float checkingDistance;
                Vector3 tempTargetObjPos = targetObject.transform.position;
                tempTargetObjPos.y = 0;
                Vector3 tempCurrentObjPos = transform.position;
                tempCurrentObjPos.y = 0;
                checkingDistance = Vector3.Distance(tempTargetObjPos, tempCurrentObjPos);
                //Debug.Log("TargetObject:" + targetObject.transform.position);
                //Debug.Log("MeleeObject:" + this.transform.position);
                //Debug.Log(checkingDistance);
                if (checkingDistance <= _attackDistance)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public void Melee()
    {
        _isAttacking = false;
        if (checkIfObjectIsNear(_player.gameObject)){
            //Debug.Log("HitPlayer");
            _player.GetComponent<PlayerProperty>().HitPlayer();
        }
    }

    
}
