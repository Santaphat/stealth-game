﻿using SensorToolkit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TurningAction
{
    Left,
    Right,
    Center
}
public enum SecuirtyState{
    Watching,
    DetectingPlayer,
    Alerted
}
public class Security_Camera_AI : BaseEnemy
{
    [SerializeField]
    protected Transform _securityCameraHead;
    [SerializeField]
    [Range(0, 180)]
    protected float MaxAnglesTurnLeft = -1;
    [SerializeField]
    [Range(0, 180)]
    protected float MaxAnglesTurnRight = -1;
    [SerializeField]
    protected float _rotationSpeedOfPos = 0.5f;
    [SerializeField]
    protected TurningAction _startingTurnDirection;

    protected float currentAnglesTurned = 0;
    protected Vector3 _leftAngleLookPos;
    protected Vector3 _rightAngleLookPos;
    protected Vector3 _centerAngleLookPos;
    private void Awake()
    {
        base.Awake();
        _enemyType = EnemyType.SecurityCamera;
    }
    protected override void Start()
    {
        base.Start();
        if(MaxAnglesTurnLeft == -1)
        {
            Debug.LogError("MaxAnglesLeft is not set");
        }
        else
        {
            _leftAngleLookPos = 3 * (Quaternion.Euler(0, (360 - MaxAnglesTurnLeft), 0) * this.transform.rotation * Vector3.forward) + _securityCameraHead.transform.position;
            //Debug.Log(Quaternion.Euler(0, (360 - MaxAnglesTurnLeft), 0) * this.transform.rotation * Vector3.forward);
        }
        if(MaxAnglesTurnRight == -1)
        {
            Debug.LogError("MaxAnglesRight is not set");
        }
        else
        {
            _rightAngleLookPos = 3 * (Quaternion.Euler(0, MaxAnglesTurnRight, 0) * this.transform.rotation * Vector3.forward) + _securityCameraHead.transform.position;
            //Debug.Log(Quaternion.Euler(0, MaxAnglesTurnRight, 0) * this.transform.rotation * Vector3.forward);
        }
        //Debug.Log(this.transform.rotation);
        _centerAngleLookPos = _securityCameraHead.position + _securityCameraHead.forward * 3;
    }

    public override bool canSeeObject(Transform _targetObject, string targetedTag, bool ignoreTrigger)
    {
        Vector3 direction = _targetObject.position - _securityCameraHead.position;
        direction.y = 0;
        float angle = Vector3.Angle(direction, _securityCameraHead.forward);
        //Debug.Log(angle);
        if (angle < _enemyFov * 0.5f)
        {
            Debug.DrawLine(_securityCameraHead.position, _targetObject.position);
            RaycastHit hit;
            Vector3 newPos = _securityCameraHead.position;
            newPos.y = +0.5f;
            QueryTriggerInteraction ignore = QueryTriggerInteraction.Collide;
            if (ignoreTrigger)
            {
                ignore = QueryTriggerInteraction.Ignore;
            }

            if (Physics.Raycast(newPos, direction, out hit, _sightDistance, _obstacleMaskRayCast, ignore))
            {
                if (hit.collider != null)
                {
                    //Debug.Log(hit.collider.gameObject);
                    if (hit.collider.tag == targetedTag)
                    {
                        //Debug.Log("Hit");
                        return true;
                    }
                }
            }
        }
        return false;
    }

    protected override void OnDrawGizmos()
    {
        base.OnDrawGizmos();
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(_leftAngleLookPos, 0.1f);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(_rightAngleLookPos, 0.1f);
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(CenterLookPos, 0.1f);
        Ray test = new Ray(_securityCameraHead.position, _securityCameraHead.forward);
        Gizmos.color = Color.red;
        Gizmos.DrawRay(test);
        
    }

    public bool TurnToPos(Vector3 target)
    {
        //find the vector pointing from our position to the target
        Vector3 _direction = (target - _securityCameraHead.transform.position).normalized;
        Quaternion tempOldRotation = _securityCameraHead.transform.rotation;

        //create the rotation we need to be in to look at the target
        Quaternion _lookRotation = Quaternion.LookRotation(_direction);
        float eularAnglesY = _securityCameraHead.transform.localEulerAngles.y;
        //Debug.Log(Mathf.Abs( _securityCameraHead.transform.eulerAngles.y - _lookRotation.eulerAngles.y));

        if (eularAnglesY > 180)
        {
            if (Mathf.Abs(_securityCameraHead.transform.eulerAngles.y - _lookRotation.eulerAngles.y) > 1.0f && eularAnglesY > (360 - MaxAnglesTurnLeft))
            {
                //Debug.Log("Inside2");
                //rotate us over time according to speed until we are in the required rotation
                Quaternion temp = Quaternion.Slerp(_securityCameraHead.transform.rotation, _lookRotation, Time.deltaTime * _rotationSpeedOfPos);
                _securityCameraHead.transform.rotation = temp;
                if (_securityCameraHead.transform.localEulerAngles.y < 360 - MaxAnglesTurnLeft && _securityCameraHead.transform.localEulerAngles.y > 180)
                {
                    _securityCameraHead.transform.rotation = tempOldRotation;
                    Debug.Log("Hello");
                    return true;
                }
                else
                {
                    _securityCameraHead.transform.localEulerAngles = new Vector3(0, _securityCameraHead.localEulerAngles.y, 0);
                    return false;
                }
            }
            else
            {
                //if it reachs the target
                return true;
            }
        }
        else
        {
            //Debug.Log("Inside1");
            if (Mathf.Abs(_securityCameraHead.transform.eulerAngles.y - _lookRotation.eulerAngles.y) > 1.0f && eularAnglesY < MaxAnglesTurnRight)
            {
                //rotate us over time according to speed until we are in the required rotation
                Quaternion temp = Quaternion.Slerp(_securityCameraHead.transform.rotation, _lookRotation, Time.deltaTime * _rotationSpeedOfPos);
                _securityCameraHead.transform.rotation = temp;
                if (_securityCameraHead.transform.localEulerAngles.y > MaxAnglesTurnRight && _securityCameraHead.transform.localEulerAngles.y < 180)
                {
                    _securityCameraHead.transform.rotation = tempOldRotation;
                    return true;
                }
                else
                {
                    _securityCameraHead.transform.localEulerAngles = new Vector3(0, _securityCameraHead.localEulerAngles.y, 0);
                    return false;
                }
            }
            else
            {
                //if it reachs the target
                return true;
            }
        }
    }

    public override bool TurnTowards(Vector3 target)
    {
        //find the vector pointing from our position to the target
        Vector3 _direction = (target - _securityCameraHead.transform.position).normalized;
        Quaternion tempOldRotation = _securityCameraHead.transform.rotation;

        //create the rotation we need to be in to look at the target
        Quaternion _lookRotation = Quaternion.LookRotation(_direction);
        float eularAnglesY = _securityCameraHead.transform.localEulerAngles.y;
        //Debug.Log(Mathf.Abs( _securityCameraHead.transform.eulerAngles.y - _lookRotation.eulerAngles.y));

        if (eularAnglesY > 180)
        {
            if (Mathf.Abs(_securityCameraHead.transform.eulerAngles.y - _lookRotation.eulerAngles.y) > 1.0f && eularAnglesY > (360 - MaxAnglesTurnLeft) )
            {
                //Debug.Log("Inside2");
                //rotate us over time according to speed until we are in the required rotation
                Quaternion temp = Quaternion.Slerp(_securityCameraHead.transform.rotation, _lookRotation, Time.deltaTime * _rotationSpeed);
                _securityCameraHead.transform.rotation = temp;
                if (_securityCameraHead.transform.localEulerAngles.y < 360 - MaxAnglesTurnLeft && _securityCameraHead.transform.localEulerAngles.y > 180)
                {
                    _securityCameraHead.transform.rotation = tempOldRotation;
                    Debug.Log("Hello");
                    return true;
                }
                else
                {
                    _securityCameraHead.transform.localEulerAngles = new Vector3(0, _securityCameraHead.localEulerAngles.y, 0);
                    return false;
                }
            }
            else
            {
                //if it reachs the target
                return true;
            }
        }
        else
        {
            //Debug.Log("Inside1");
            if (Mathf.Abs(_securityCameraHead.transform.eulerAngles.y - _lookRotation.eulerAngles.y) > 1.0f && eularAnglesY < MaxAnglesTurnRight)
            {
                //rotate us over time according to speed until we are in the required rotation
                Quaternion temp = Quaternion.Slerp(_securityCameraHead.transform.rotation, _lookRotation, Time.deltaTime * _rotationSpeed);
                _securityCameraHead.transform.rotation = temp;
                if (_securityCameraHead.transform.localEulerAngles.y > MaxAnglesTurnRight && _securityCameraHead.transform.localEulerAngles.y < 180)
                {
                    _securityCameraHead.transform.rotation = tempOldRotation;
                    return true;
                }
                else
                {
                    _securityCameraHead.transform.localEulerAngles = new Vector3(0, _securityCameraHead.localEulerAngles.y, 0);
                    return false;
                }
            }
            else
            {
                //if it reachs the target
                return true;
            }
        }
    }

    public Vector3 LeftLookPos
    {
        get
        {
            return _leftAngleLookPos;
        }
    }

    public Vector3 RightLookPos
    {
        get
        {
            return _rightAngleLookPos;
        }
    }

    public Vector3 CenterLookPos
    {
        get
        {
            return _centerAngleLookPos;
        }
    }

    public Transform GetSecurityCameraHead
    {
        get
        {
            return _securityCameraHead;
        }
    }

    public TurningAction startingTurnDirection
    {
        get
        {
            return _startingTurnDirection;
        }
    }
        
}
