﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
[CreateAssetMenu(fileName = "FiniteStateMachine", menuName = "Unity-FSM/FiniteStateMachine", order = 6)]
public class FiniteStateMachine : ScriptableObject
{
    [SerializeField]
    SubStateChildType _startingState;

    [SerializeField]
    List<SubChildBaseState> _validStates;
    SubChildBaseState _currentState;
    SubChildBaseState _previousState;

    Dictionary<SubStateChildType, SubChildBaseState> _FSMState;

    public void Initialize(NavMeshAgent navMeshAgent, BaseEnemy baseEnemy, Animator AC, EnemyFSM enemyFSM)
    {
        _currentState = null;

        _FSMState = new Dictionary<SubStateChildType, SubChildBaseState>();


        foreach (SubChildBaseState state in _validStates)
        {
            if (state != null)
            {
                var temp = Instantiate(state);
                temp.SetExecutingFSM(this);
                temp.SetNavMeshAgent(navMeshAgent);
                temp.SetExecutingEnemy(baseEnemy);
                temp.SetExecutingAnimator(AC);
                temp.SetTopFSM(enemyFSM);
                _FSMState.Add(temp.StateType, temp);
            }
        }

    }
    public void Start()
    {
        //if (_startingState != null)
        //{
        //    EnterState(_startingState);
        //}
    }

    public void Update()
    {
        if (_currentState != null)
        {
            _currentState.UpdateState();
        }
    }

    //public FSMStateType CurrentStateType
    //{
    //    get
    //    {
    //        return _currentState.StateType;
    //    }
    //}

    #region StateManagemnt

    public void EnterState(SubChildBaseState nextState)
    {
        if (nextState == null)
        {
            return;
        }
        if (_currentState != null)
        {
            _currentState.ExitState();
        }
        _previousState = _currentState;
        _currentState = nextState;
        _currentState.EnterState();
    }

    public void EnterState(SubStateChildType stateType)
    {
        if (_FSMState.ContainsKey(stateType))
        {
            SubChildBaseState nextState = _FSMState[stateType];

            EnterState(nextState);
        }
        else
        {
            Debug.LogError("Cannot Find " + stateType + " Inside FSM");
        }
    }

    public void ExitState()
    {

    }
    #endregion
}
