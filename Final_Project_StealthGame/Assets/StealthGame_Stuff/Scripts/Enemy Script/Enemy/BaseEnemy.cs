﻿using SensorToolkit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum baseEnemyState{
    Normal,
    Distracted,
    Suspicious,
    Alerted,
    Combat,
    Dead
}

//Base Enemy 
//Has field of view and finite state machine 
//Dont have hearing or animation or a body
[RequireComponent(typeof(EnemyFSM), typeof(FieldOfView))]
abstract public class BaseEnemy : MonoBehaviour
{
    #region Serialize Variable
    [SerializeField]
    protected bool _showDebugState;
    [SerializeField]
    protected Mesh PlayerLastKnownLocationMesh;

    [SerializeField]
    protected SphereCollider _triggerCollider;

    [SerializeField]
    protected TriggerSensor _triggerSensor;

    [SerializeField]
    protected LayerMask _obstacleMaskFieldOfView;

    [SerializeField]
    protected LayerMask _obstacleMaskRayCast;

    [SerializeField]
    [Range(0, 360)]
    protected int _enemyFov;

    [SerializeField]
    protected float _sightDistance;

    [SerializeField]
    protected float _rotationSpeed;

    //[SerializeField]
    //protected ProgressBar _alertBar;

    [SerializeField]
    protected TimeManager _timeManager;

    [SerializeField]
    protected Color _idleAlertColor;

    [SerializeField]
    protected Color _suspiciousAlertColor;

    [SerializeField]
    protected Color _AlertColor;

    [SerializeField]
    protected float _maxTimerBeforeAlertDecrease;

    [SerializeField]
    protected float _scalingIncreasingAlertFactor = 0.002f;

    [SerializeField]
    protected float _scalingDecreaseAlertFactor = 0.001f;

    //How long the player has before ai walks towards last known position
    [SerializeField]
    [Range(0, 1)]
    protected float _distancePercentageBeforeSuspicious;

    [SerializeField]
    protected float _attackRange = 1.0f;

    #endregion

    #region Unseriablize Variable
    //IncreasingAlertFactor & DecreasingAlertFactor determine how much CurrentAlertDistance scale
    protected float _increasingAlertFactor = 1;
    protected float _decreasingAlertFactor = 1;
    //PlayerDistance is the current distance between enemy and player
    protected float _playerDistance;
    //CurrentAlertDistance is the distance the alert bar is gone;
    protected float _currentAlertDistance;

    protected List<GameObject> _insideInteractableGuardRadius;

    protected bool _alertIndicatorOn;

    protected bool _firstSeenPlayer;

    protected int _currentPointIndex = -1;

    protected float _currentTimerBeforeAlertDecrease;


    //Declaration for stuff enemy should know
    protected EnemyType _enemyType;
    protected EnemyFSM _finiteStateMachine;

    protected Transform _player;
    protected Vector3 _playerLastKnownLocation;
    protected Quaternion _playerLastKnownOrientation;
    protected Vector3 _lookatObjectOfInterest;

    protected FieldOfView _FOV;
    protected Material _enemyFOVMAT;

    protected baseEnemyState _currentState;
    protected baseEnemyState _nextState;
    protected bool _isAttacking;
    protected bool isHumaniod = false;
    protected GameManager _gm;
    protected SoundEffectManager _sem;

    #endregion
    protected virtual void Awake()
    {
        //_player = FindObjectOfType<TempPlayerController>().transform;
        _finiteStateMachine = this.GetComponent<EnemyFSM>();
        _FOV = this.GetComponent<FieldOfView>();
        _gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        if(_gm == null)
        {
            Debug.Log("Cannot Find Game Manager");
        }
        if (_FOV == null)
        {
            Debug.LogError("Please add field of view script in the enemy class");
        }
        _sem = this.GetComponent<SoundEffectManager>();
        if(_sem == null)
        {
            Debug.LogError("Cannot Find Enemy Sound Manager");
        }
        _player = GameObject.Find("PlayerSetting").transform;
        if(_player == null)
        {
            Debug.LogError("Cannot find player in map");
        }
    }

    protected virtual void Start()
    {
        //if (_alertBar != null)
        //{
        //    TurnOnAlertIndicator();
        //    _alertBar.color = _idleAlertColor;
        //    _alertBar.maxium = 1;
        //    _alertBar.current = 1;
        //    _alertBar.updateBar();
        //}
        if (_FOV != null)
        {
            _FOV.viewRadius = _sightDistance;
            _FOV.viewAngle = _enemyFov;
            _FOV.obstacleMask = _obstacleMaskFieldOfView;
            _enemyFOVMAT = _FOV.viewMeshRenderer.material;
            _enemyFOVMAT.SetFloat("_FullPercent", 0.0f);
            _enemyFOVMAT.SetColor("_MainColor", _suspiciousAlertColor);
            _enemyFOVMAT.SetColor("_SecondColor", _idleAlertColor);
        }
        _currentAlertDistance = 0;
        _nextState = baseEnemyState.Normal;
    }

    protected virtual void Update()
    {
        if(_currentState != _nextState && _currentState != baseEnemyState.Dead)
        {
            _currentState = _nextState;
        }
    }

    protected virtual void OnDrawGizmos()
    {
        if (_playerLastKnownLocation != null && _playerLastKnownLocation != Vector3.zero)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawMesh(PlayerLastKnownLocationMesh, _playerLastKnownLocation, Quaternion.identity, Vector3.one);
        }

    }

    public EnemyType getEnemyType()
    {
        return _enemyType;
    }

    public virtual void enemyGotHit()
    {
        _nextState = baseEnemyState.Dead;
        _FOV.viewMeshRenderer.gameObject.SetActive(false);
        _finiteStateMachine.EnterState(FSMStateType.DeadState);
    }

    public virtual void afterDeathAnimation()
    {
        Destroy(this.gameObject);
    }

    //public void setAlertColor(Color color)
    //{
    //    _alertBar.color = color;
    //    _alertBar.updateBar();
    //}

    public void TurnOnAlertIndicator()
    {
        _alertIndicatorOn = true;
        //turn on view

    }
    public void TurnOffAlertIndicator()
    {
        _alertIndicatorOn = false;
        //turn off view
    }

    public virtual bool Attacking()
    {
        return false;
    }

    public virtual bool Attacking(Transform target)
    {
        return false;
    }

    public virtual bool TurnTowards(Vector3 target)
    {

        //find the vector pointing from our position to the target
        Vector3 _direction = (target - transform.position).normalized;

        //create the rotation we need to be in to look at the target
        Quaternion _lookRotation = Quaternion.LookRotation(_direction);
        if (Vector3.Distance(this.transform.eulerAngles, _lookRotation.eulerAngles) > 1.0f)
        {
            //rotate us over time according to speed until we are in the required rotation
            Quaternion temp = Quaternion.Slerp(transform.rotation, _lookRotation, Time.deltaTime * _rotationSpeed);
            transform.rotation = temp;
            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
            return false;
        }
        else
        {
            return true;
        }
    }

    public virtual bool TurnOrientationToObject(Transform target)
    {
        if (Vector3.Distance(this.transform.eulerAngles, target.eulerAngles) > 1.0f)
        {
            Quaternion temp = Quaternion.Slerp(transform.rotation, target.rotation, Time.deltaTime * _rotationSpeed);
            transform.rotation = temp;
            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
            return false;
        }
        else
        {
            return true;
        }
    }
    //Have to fix when player has trigger
    public abstract bool canSeeObject(Transform _targetObject, string targetedTag, bool ignoreTrigger);

    public Vector3 GetRandomPositionInArea(Vector3 origin, float sphereRadius, int layerMask)
    {
        Vector3 randomDirection = UnityEngine.Random.insideUnitSphere * sphereRadius;
        randomDirection += origin;
        NavMeshHit navHit;
        NavMesh.SamplePosition(randomDirection, out navHit, sphereRadius, NavMesh.AllAreas);
        return navHit.position;
    }

    public void Alerted()
    {
        CurrentAlertSightDistance = _sightDistance;
        _nextState = baseEnemyState.Alerted;
        _enemyFOVMAT.SetColor("_MainColor", _AlertColor);
        _enemyFOVMAT.SetColor("_SecondColor", _AlertColor);
    }

    public void NonAlerted()
    {
        _nextState = baseEnemyState.Normal;
        _enemyFOVMAT.SetColor("_MainColor", _suspiciousAlertColor);
        _enemyFOVMAT.SetColor("_SecondColor", _idleAlertColor);
    }

    #region Set/Get Variable

    public GameManager GameManager
    {
        get
        {
            return _gm;
        }
    }
    public baseEnemyState CurrentState
    {
        get
        {
            return _currentState;
        }
    }
    public int CurrentPatrolIndex
    {
        get
        {
            return _currentPointIndex;
        }
        set
        {
            _currentPointIndex = value;
        }
    }
    public bool FirstSeenPlayer
    {
        get
        {
            return _firstSeenPlayer;
        }
        set
        {
            _firstSeenPlayer = value;
        }
    }

    public float EnemySightDistance
    {
        get
        {
            return _sightDistance;
        }
    }

    //Setting set and get
    public Vector3 ObjectOfInterest
    {
        get
        {
            return _lookatObjectOfInterest;
        }
        set
        {
            _lookatObjectOfInterest = value;
        }
    }

    public float PlayerDistance
    {
        get
        {
            return _playerDistance;
        }
        set
        {
            _playerDistance = value;
        }
    }

    public float CurrentTimerBeforeAlertDecrease
    {
        get
        {
            return _currentTimerBeforeAlertDecrease;
        }
        set
        {
            _currentTimerBeforeAlertDecrease = value;
        }
    }

    public float MaxTimerBeforeAlertDecrease
    {
        get
        {
            return _maxTimerBeforeAlertDecrease;
        }
    }

    public float CurrentAlertSightDistance
    {
        get
        {
            return _currentAlertDistance;
        }
        set
        {
            _currentAlertDistance = value;
            //Debug.Log(_currentAlertDistance / _playerDistance);
            _enemyFOVMAT.SetFloat("_FullPercent", _currentAlertDistance / _sightDistance);
        }
    }

    public virtual bool GetIsHumaniod()
    {
        return isHumaniod;
    }

    public float IncreasingAlertFactor
    {
        get
        {
            return _increasingAlertFactor;
        }
        set
        {
            _increasingAlertFactor += value * _scalingIncreasingAlertFactor;
        }
    }

    public float DecreasingAlertScaling
    {
        get
        {
            return _decreasingAlertFactor;
        }
        set
        {
            _decreasingAlertFactor += value * _scalingDecreaseAlertFactor;
        }
    }

    public TriggerSensor EnemyTriggerSensor
    {
        get
        {
            return _triggerSensor;
        }
    }

    public Vector3 PlayerLastKnownPosition
    {
        get
        {
            return _playerLastKnownLocation;
        }
        set
        {
            _playerLastKnownLocation = value;
        }

    }

    public Quaternion PlayerLastKnownOrientation
    {
        get
        {
            return _playerLastKnownOrientation;
        }
        set
        {
            _playerLastKnownOrientation = value;
        }
    }

    public bool AlertIndicatorOn
    {
        get
        {
            return _alertIndicatorOn;
        }
    }

    public float AttackRange
    {
        get
        {
            return _attackRange;
        }
    }

    public bool GetIsAttacking
    {
        get
        {
            return _isAttacking;
        }
        set
        {
            _isAttacking = value;
        }
    }

    public bool ShowDebug
    {
        get
        {
            return _showDebugState;
        }
    }

    public SoundEffectManager EnemySoundEffectManager
    {
        get
        {
            return _sem;
        }
    }
    #endregion
}
