﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeGuard : NormalGuard
{
    [SerializeField]
    Transform _bulletExitPosition;
    [SerializeField]
    GameObject _bulletPrefab;
    [SerializeField]
    float _bulletSpeed;

    protected override void Awake()
    {
        base.Awake();
        _enemyType = EnemyType.RangeGuard;
    }
    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }
    public override bool Attacking()
    {
        //Debug.Log("Enter Attack");
        bool attackSuccessful = true;
        _isAttacking = true;
        _AC.SetTrigger("Attack");
        //StartCoroutine("FireThreeShot");
        return attackSuccessful;
    }

    public void FireBullet()
    {
        _sem.PlaySoundEffect("Attack");
        _isAttacking = false;
        Quaternion rotation = Quaternion.LookRotation(_playerLastKnownLocation, Vector3.up);
        rotation.eulerAngles = new Vector3(0, rotation.eulerAngles.y, 0);
        Vector3 direction = (_playerLastKnownLocation - _bulletExitPosition.position).normalized;
        direction = new Vector3(direction.x, 0, direction.z);
        GameObject tempBullet = Instantiate(_bulletPrefab, _bulletExitPosition.position, rotation);
        tempBullet.SetActive(true);
        tempBullet.GetComponent<Rigidbody>().AddForce(direction * _bulletSpeed, ForceMode.Impulse);
    }


    //protected IEnumerator FireThreeShot()
    //{
    //    _AC.SetTrigger("Attack");
    //    yield return new WaitForSeconds(0.25f);
    //    //FireBullet();
    //    //yield return new WaitForSeconds(0.25f);
    //    //FireBullet();
    //    //yield return new WaitForSeconds(0.25f);
    //}
}
