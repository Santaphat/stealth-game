﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "SearchAreaState", menuName = "Unity-FSM/State/SearchAreaState", order = 5)]
public class SearchAreaState : BaseState
{

    bool _firstSearch;
    [SerializeField]
    float _searchSphereArea;
    public override void OnEnable()
    {
        base.OnEnable();
        StateType = FSMStateType.SEARCHAREA;
    }
    public override bool EnterState()
    {
        EnteredState = true;
        if (base.EnterState())
        {
            if (_baseEnemy.GetIsHumaniod())
            {
                _humaniodEnemy.SetDestination(_baseEnemy.PlayerLastKnownPosition);
                _humaniodEnemy.StartMovement(_moveSpeed);
            }
            _baseEnemy.CurrentTimerBeforeAlertDecrease = _baseEnemy.MaxTimerBeforeAlertDecrease;
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log("Entering SearchAreaState State");
            }
            _baseEnemy.GameManager.EnemyAlerted();
        }
        else
        {
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log("Cannot Enter SearchAreaState State");
            }
            EnteredState = false;
        }

        return EnteredState;
    }

    public override bool ExitState()
    {
        if (base.ExitState())
        {
            _baseEnemy.NonAlerted();
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log("Exiting SearchAreaState State");
            }
            _baseEnemy.GameManager.EnemyUnAlerted();
            return true;
        }
        else
        {
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log("Cannot Exit SearchAreaState State");
            }
            return false;
        }
    }

    public override void UpdateState()
    {
        if (EnteredState)
        {
            //Debug.Log(_baseEnemy.CurrentTimerBeforeAlertDecrease);
            List<GameObject> objectDetected = _triggerSensor.GetDetected();
            bool seePlayer = false;
            if (objectDetected.Count > 0)
            {
                foreach (GameObject objectSense in objectDetected)
                {
                    if (objectSense.tag == "Player")
                    {
                        BodyParts temp = objectSense.GetComponent<BodyParts>();
                        if (temp)
                        {
                            if (_baseEnemy.canSeeObject(temp.chest, "Player", true))
                            {
                                seePlayer = true;
                                PlayerInSight(objectSense.transform);
                            }
                        }
                    }
                }
            }
            if (_baseEnemy.GetIsHumaniod())
            {
                if (_navMeshAgent != null)
                {

                    if (_navMeshAgent.remainingDistance <= 0.1f)
                    {

                        _humaniodEnemy.SetDestination(_baseEnemy.GetRandomPositionInArea(_baseEnemy.PlayerLastKnownPosition, _searchSphereArea, 1 << 1));
                        _humaniodEnemy.StartMovement(_moveSpeed);
                    }
                }
            }
            if(_baseEnemy.CurrentTimerBeforeAlertDecrease < 0)
            {
                _enemyFSM.EnterState(FSMStateType.NonAlertState);
            }
            if (_baseEnemy.CurrentTimerBeforeAlertDecrease >= 0)
            {
                _baseEnemy.CurrentTimerBeforeAlertDecrease -= Time.deltaTime;
            }


        }
    }

    protected override void PlayerInSight(Transform player)
    {
        _baseEnemy.CurrentAlertSightDistance = _baseEnemy.PlayerDistance;
        _enemyFSM.EnterState(FSMStateType.COMBAT);
    }
}
