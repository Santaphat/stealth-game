﻿//using UnityEngine;
//using UnityEditor;
//using System.Collections.Generic;

//[CreateAssetMenu(fileName = "IdleState", menuName = "Unity-FSM/State/IdleState", order = 1)]
//public class IdleState : BaseState
//{
//    [SerializeField]
//    float _idleDuration = 3.0f;
   
//    float _totalDuration;

//    public override void OnEnable()
//    {
//        base.OnEnable();
//        StateType = FSMStateType.IDLE;
//    }
//    public override bool EnterState()
//    {
//        EnteredState = true;
//        if (base.EnterState())
//        {
//            _baseEnemy.setAlertColor(Color.white);
//            _baseEnemy.FirstSeenPlayer = false;
//            _totalDuration = 0.0f;
//            _baseEnemy.StopMovement();
//            Debug.Log(_baseEnemy.gameObject.name +  " Entering Idle State");
//        }
//        else
//        {
//            Debug.Log("Cannot Enter Idle State");
//            EnteredState = false;
//        }

//        return EnteredState;
//    }

//    public override bool ExitState()
//    {
//        if (base.ExitState())
//        {
//            Debug.Log(_baseEnemy.gameObject.name +  " Exiting Idle State");
//            return true;
//        }
//        else
//        {
//            Debug.Log("Cannot Exit Idle State");
//            return false;
//        }
//    }

//    public override void UpdateState()
//    {
//        if (EnteredState)
//        {
//            if (_currentTimerBeforeAlertDecrease > 0)
//            {
//                _currentTimerBeforeAlertDecrease -= Time.deltaTime;
//            }
//            else
//            {
//                if (_baseEnemy.CurrentAlertSightDistance > 0)
//                {
//                    _baseEnemy.CurrentAlertSightDistance -= Time.deltaTime;
//                }
//                else
//                {
//                    _baseEnemy.FirstSeenPlayer = false;
//                }
//            }
//            //If player enter ai field of view it will go to Suspicious State
//            List<GameObject> objectDetected = _triggerSensor.GetDetected();
//            if (objectDetected.Count > 0)
//            {
//                foreach (GameObject objectSense in objectDetected)
//                {
//                    if (objectSense.tag == "Player")
//                    {
//                        if (_baseEnemy.canSeeObject(objectSense.transform, true))
//                        {
//                            PlayerInSight(objectSense.transform);
//                        }
//                    }
//                }
//            }

//            _totalDuration += Time.deltaTime;
//            //Debug.Log("Idle Timer" + _totalDuration + " seconds.");
//            if (_totalDuration >= _idleDuration)
//            {
//                _enemyFSM.EnterState(FSMStateType.NONEALERTPATROL);
//            }
//        }
//        //Debug.Log("Inside Idle State");
//    }

//    protected override void PlayerInSight(Transform player)
//    {
//        if (!_baseEnemy.FirstSeenPlayer)
//        {
//            _baseEnemy.FirstSeenPlayer = true;
//            //if (_baseEnemy.TimeMangerObject != null)
//            //{
//            //    _baseEnemy.TimeMangerObject.DoSlowMotion();
//            //}
//        }
//        _currentTimerBeforeAlertDecrease = _maxTimerBeforeAlertDecrease;
//        if (!_baseEnemy.AlertIndicatorOn)
//        {
//            _baseEnemy.TurnOnAlertIndicator();
//        }
//        if (_baseEnemy.CurrentAlertSightDistance < _baseEnemy.DistanceBeforeSuspicious)
//        {
//            //_baseEnemy.CurrentTimerBeforeAlert += Time.deltaTime * (seePlayerAlertScaleTimer * ( 1 - ((Vector3.Distance(player.position, _baseEnemy.transform.position))/ _baseEnemy.SightDistance)));
//        }
//        else
//        {
//            _baseEnemy.PlayerLastKnownPosition = player.position;
//            _enemyFSM.EnterState(FSMStateType.SUSPICIOUS);
//        }
//    }

//}