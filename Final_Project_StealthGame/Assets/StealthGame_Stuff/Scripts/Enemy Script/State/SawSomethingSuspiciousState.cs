﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "SawSomethingSuspiciousState", menuName = "Unity-FSM/State/SawSomethingSuspiciousState", order = 1)]
public class SawSomethingSuspiciousState : BaseState
{
    [SerializeField]
    float _maxWaitTime;

    bool _onLastPlayerKnownLocation;
    float _currentWaitTime;

    public override void OnEnable()
    {
        base.OnEnable();
        StateType = FSMStateType.SawSomethingSuspiciousState;
    }
    public override bool EnterState()
    {
        EnteredState = true;
        if (base.EnterState())
        {
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log(_baseEnemy.gameObject.name + " Entering SawSomething Suspicious State");
            }
            if (_baseEnemy.GetIsHumaniod())
            {
                if (_baseEnemy.PlayerLastKnownPosition != null && _baseEnemy.PlayerLastKnownPosition != Vector3.zero)
                {
                    _humaniodEnemy.SetDestination(_baseEnemy.PlayerLastKnownPosition);
                    _humaniodEnemy.StartMovement(_moveSpeed);
                    _onLastPlayerKnownLocation = false;
                    _currentWaitTime = 0.0f;
                }
                else
                {
                    if (_baseEnemy.ShowDebug)
                    {
                        Debug.LogError("Player havent been detected something went wrong when entering state");
                    }
                    EnteredState = false;
                }
            }
            else
            {
                if (_baseEnemy.ShowDebug)
                {
                    Debug.LogError("Player havent been detected something went wrong when entering state");
                }
                EnteredState = false;
            }
        }
        else
        {
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log("Cannot Enter SawSomething Suspicious State");
            }
            EnteredState = false;
        }

        return EnteredState;
    }

    public override bool ExitState()
    {
        if (base.ExitState())
        {
            _baseEnemy.PlayerLastKnownPosition = Vector3.zero;
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log(_baseEnemy.gameObject.name + " Exiting SawSomething Suspicious State");
            }
            return true;
        }
        else
        {
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log("Cannot Exit SawSomething Suspicious State");
            }
            return false;
        }
    }
    public override void UpdateState()
    {
        if (EnteredState)
        {
            List<GameObject> objectDetected = _triggerSensor.GetDetected();
            if (objectDetected.Count > 0)
            {
                foreach (GameObject objectSense in objectDetected)
                {
                    if (objectSense.tag == "Player")
                    {
                        BodyParts temp = objectSense.GetComponent<BodyParts>();
                        if (temp)
                        {
                            if (_baseEnemy.canSeeObject(temp.chest, "Player", true))
                            {
                                PlayerInSight(objectSense.transform);
                            }
                        }
                        else
                        {
                            Debug.Log("Please put bodyparts script inside player for better raycast");
                            if (_baseEnemy.canSeeObject(objectSense.transform, "Player", true))
                            {
                                PlayerInSight(objectSense.transform);
                            }
                        }
                    }
                }
            }
            if (_baseEnemy.GetIsHumaniod())
            {
                if (_navMeshAgent.remainingDistance <= 0.1f && !_onLastPlayerKnownLocation)
                {
                    //Debug.Log("Entered");
                    if (!_onLastPlayerKnownLocation)
                    {
                        _onLastPlayerKnownLocation = true;
                        _humaniodEnemy.StopMovement();
                        _currentWaitTime = _maxWaitTime;
                    }
                }
                else if (_onLastPlayerKnownLocation)
                {
                    //Debug.Log("Entered2");
                    if (_baseEnemy.CurrentTimerBeforeAlertDecrease > 0)
                    {
                        _baseEnemy.CurrentTimerBeforeAlertDecrease -= Time.deltaTime;
                    }
                    else if (_baseEnemy.CurrentAlertSightDistance > 0)
                    {
                        _baseEnemy.CurrentAlertSightDistance -= Time.deltaTime * _scaleDecreaseDistanceAlert;
                    }
                    if (LookAround())
                    {
                        _enemyFSM.EnterState(FSMStateType.NonAlertState);
                    }
                }
            }
        }
        //Debug.Log(_currentWaitTime);
    }
    bool LookAround()
    {
        _currentWaitTime -= Time.deltaTime;
        if (_currentWaitTime <= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
        ////Look left
        //if (!_lookingLeft)
        //{

        //}
        ////Look Right
        //else if (!_lookingRight)
        //{

        //}
        //if (_lookingLeft && _lookingRight)
        //{
        //    return true;
        //}
        //else
        //{
        //    return false;
        //}
    }

    protected override void PlayerInSight(Transform player)
    {
        base.PlayerInSight(player);
        if (Vector3.Distance(_baseEnemy.transform.position, player.position) < _baseEnemy.EnemySightDistance * 0.8f)
        {
            _enemyFSM.EnterState(FSMStateType.DetectingPlayer);
        }
        if (_baseEnemy.CurrentAlertSightDistance <= _baseEnemy.PlayerDistance)
        {
            _baseEnemy.CurrentAlertSightDistance += Time.deltaTime * _scaleDecreaseDistanceAlert;
        }
    }
}
