﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "HeardSomethingState", menuName = "Unity-FSM/State/HeardSomethingState", order = 1)]
public class HeardSomethingState : BaseState
{
    [SerializeField]
    float _maxWaitTime;
    float _currentWaitTime;

    Vector3 currentSoundLocation;
    public override void OnEnable()
    {
        base.OnEnable();
        StateType = FSMStateType.HeardSomethingState;
    }
    public override bool EnterState()
    {
        EnteredState = true;
        if (base.EnterState())
        {
            if (!_baseEnemy.GetIsHumaniod())
            {
                if (_baseEnemy.ShowDebug)
                {
                    Debug.LogError("HeardSomethingState is entered when enemy is not humaniod");
                }

                EnteredState = false;
            }
            else if (_humaniodEnemy.HeardSomethingPosition != null)
            {
                if (_baseEnemy.ShowDebug)
                {
                    Debug.Log(_baseEnemy.gameObject.name + " Properly Entered Heard Something state");
                }
                _baseEnemy.EnemySoundEffectManager.PlaySoundEffect("EnemyReactionToSound");
                _humaniodEnemy.SetDestination(_humaniodEnemy.HeardSomethingPosition);
                currentSoundLocation = _humaniodEnemy.HeardSomethingPosition;
                _humaniodEnemy.StartMovement(_moveSpeed);
                _currentWaitTime = 0.0f;
            }
            else
            {
                if (_baseEnemy.ShowDebug)
                {
                    Debug.LogError("Enter state wrongly || HeardSomethingState");
                }
            }
        }
        else
        {
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log("Cannot Enter HeardSomethingState");
            }
            EnteredState = false;
        }

        return EnteredState;
    }

    public override bool ExitState()
    {
        if (base.ExitState())
        {
            if (_humaniodEnemy.HeardSomethingPosition == currentSoundLocation)
            {
                _humaniodEnemy.HeardSomethingPosition = Vector3.zero;
            }
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log(_baseEnemy.gameObject.name + " Exiting HeardSomethingState");
            }
            return true;
        }
        else
        {
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log("Cannot Exit HeardSomethingState");
            }
            return false;
        }
    }
    public override void UpdateState()
    {
        if (EnteredState)
        { 
            List<GameObject> objectDetected = _triggerSensor.GetDetected();
            if (objectDetected.Count > 0)
            {
                foreach (GameObject objectSense in objectDetected)
                {
                    if (objectSense.tag == "Player")
                    {
                        BodyParts temp = objectSense.GetComponent<BodyParts>();
                        if (temp)
                        {
                            if (_baseEnemy.canSeeObject(temp.chest, "Player", true))
                            {
                                PlayerInSight(objectSense.transform);
                            }
                        }
                        else
                        {
                            Debug.Log("Please put bodyparts script inside player for better raycast");
                            if (_baseEnemy.canSeeObject(objectSense.transform, "Player", true))
                            {
                                PlayerInSight(objectSense.transform);
                            }
                        }
                    }
                }
            }
            if (_navMeshAgent.remainingDistance <= 0.1f)
            {
                if (_baseEnemy.CurrentTimerBeforeAlertDecrease > 0)
                {
                    _baseEnemy.CurrentTimerBeforeAlertDecrease -= Time.deltaTime;
                }
                else if (_baseEnemy.CurrentAlertSightDistance > 0)
                {
                    _baseEnemy.CurrentAlertSightDistance -= Time.deltaTime * _scaleDecreaseDistanceAlert;
                }
                if (LookAround())
                {
                    _enemyFSM.EnterState(FSMStateType.NonAlertState);
                }
            }
        }
    }

    protected override void PlayerInSight(Transform player)
    {
        base.PlayerInSight(player);
        if (Vector3.Distance(_baseEnemy.transform.position, player.position) < _baseEnemy.EnemySightDistance * 0.8f)
        {
            _enemyFSM.EnterState(FSMStateType.DetectingPlayer);
        }
        if (_baseEnemy.CurrentAlertSightDistance <= _baseEnemy.PlayerDistance)
        {
            _baseEnemy.CurrentAlertSightDistance += Time.deltaTime * _scaleDecreaseDistanceAlert;
        }
    }

    bool LookAround()
    {
        _currentWaitTime -= Time.deltaTime;
        if (_currentWaitTime <= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
        ////Look left
        //if (!_lookingLeft)
        //{

        //}
        ////Look Right
        //else if (!_lookingRight)
        //{

        //}
        //if (_lookingLeft && _lookingRight)
        //{
        //    return true;
        //}
        //else
        //{
        //    return false;
        //}
    }
}
