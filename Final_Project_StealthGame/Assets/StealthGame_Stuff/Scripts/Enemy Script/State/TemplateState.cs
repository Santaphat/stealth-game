﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "TemplateState", menuName = "Unity-FSM/State/TemplateState", order = 1)]
public class TemplateState : BaseState
{
    public override void OnEnable()
    {
        base.OnEnable();
        StateType = FSMStateType.TEST;
    }
    public override bool EnterState()
    {
        EnteredState = true;
        if (base.EnterState())
        {
            Debug.Log(_baseEnemy.gameObject.name + " Entering Template State");
        }
        else
        {
            Debug.Log("Cannot Enter Idle State");
            EnteredState = false;
        }

        return EnteredState;
    }

    public override bool ExitState()
    {
        if (base.ExitState())
        {
            Debug.Log(_baseEnemy.gameObject.name + " Exiting Template State");
            return true;
        }
        else
        {
            Debug.Log("Cannot Exit Template State");
            return false;
        }
    }
    public override void UpdateState()
    {
        throw new System.NotImplementedException();
    }

    protected override void PlayerInSight(Transform player)
    {
        throw new System.NotImplementedException();
    }
}
