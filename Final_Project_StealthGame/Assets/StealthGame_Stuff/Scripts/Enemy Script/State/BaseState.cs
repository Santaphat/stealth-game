﻿using SensorToolkit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum ExectuionState
{
    NONE,
    ACTIVE,
    COMPLETED,
    TERMINATED,
};

public enum FSMStateType
{
    NonAlertState,
    SawSomethingSuspiciousState,
    DetectingPlayer,
    HeardSomethingState,
    //IDLE,
    //NONEALERTPATROL,
    //GUARD,
    //SUSPICIOUS,
    COMBAT,
    SEARCHAREA,
    TEST,
    DeadState
}

abstract public class BaseState : ScriptableObject
{
    [SerializeField]
    protected float _moveSpeed;

    [SerializeField]
    protected float _scaleDecreaseDistanceAlert = 1;

    public ExectuionState ExectuionState { get; protected set; }
    protected NavMeshAgent _navMeshAgent;
    protected BaseEnemy _baseEnemy;
    protected HumaniodEnemy _humaniodEnemy;
    protected EnemyFSM _enemyFSM;
    protected EnemyType ET;
    protected Animator _AC;
    protected TriggerSensor _triggerSensor;

    public FSMStateType StateType { get; protected set; }

    public bool EnteredState { get; protected set; }

    public virtual void UpdateState()
    {
        //RangeGuard temp = (RangeGuard)(NormalGuard)_baseEnemy;
        if (_baseEnemy.CurrentTimerBeforeAlertDecrease > 0)
        {
            _baseEnemy.CurrentTimerBeforeAlertDecrease -= Time.deltaTime;
        }
        else if(_baseEnemy.CurrentAlertSightDistance > 0)
        {
            _baseEnemy.CurrentAlertSightDistance -= Time.deltaTime * _baseEnemy.DecreasingAlertScaling;
            _baseEnemy.DecreasingAlertScaling += Time.deltaTime;
        }
        if (_baseEnemy.CurrentAlertSightDistance <= 0) {
            _baseEnemy.IncreasingAlertFactor = 1;
        }
    }
    protected virtual void PlayerInSight(Transform player)
    {
        _baseEnemy.DecreasingAlertScaling = 1;
        _baseEnemy.ObjectOfInterest = player.position;
        _baseEnemy.PlayerLastKnownPosition = player.position;
        _baseEnemy.PlayerLastKnownOrientation = player.rotation;
        _baseEnemy.PlayerDistance = Vector3.Distance(player.transform.position, _baseEnemy.transform.position);
        _baseEnemy.CurrentTimerBeforeAlertDecrease = _baseEnemy.MaxTimerBeforeAlertDecrease;
    }

    public virtual void OnEnable()
    {
        ExectuionState = ExectuionState.NONE;
    }
    public virtual bool EnterState()
    {

        bool successBaseEnemy = true;
        _baseEnemy.CurrentTimerBeforeAlertDecrease = _baseEnemy.MaxTimerBeforeAlertDecrease;
        ExectuionState = ExectuionState.ACTIVE;
        successBaseEnemy = (_baseEnemy != null);
        if (!successBaseEnemy)
        {
            Debug.LogError("Cannot find baseEnemy");
        }
        if (_baseEnemy.GetIsHumaniod()) {
            bool successAnimator = true;
            bool successNavMesh = true;
            bool successHumaniod = true;
            successNavMesh = (_navMeshAgent != null);
            successAnimator = (_AC != null);
            successHumaniod = (_humaniodEnemy != null);
            if (!successAnimator)
            {
                Debug.LogError("Cannot find animator");
            }
            if (!successHumaniod)
            {
                Debug.LogError("Cannot find humaniod");
            }
            if (!successNavMesh)
            {
                Debug.LogError("Cannot find navmesh");
            }
            return successNavMesh & successBaseEnemy & successAnimator & successHumaniod;
        }
        return  successBaseEnemy;
    }

    public virtual bool ExitState()
    { 
        ExectuionState = ExectuionState.COMPLETED;
        return true;
    }

    public virtual void SetHumaniodBase(HumaniodEnemy humaniodEnemy)
    {
        if (_baseEnemy.GetIsHumaniod())
        {
            if (humaniodEnemy != null)
            {
                _humaniodEnemy = humaniodEnemy;
            }
        }
    }
    public virtual void SetNavMeshAgent(NavMeshAgent navMeshAgent)
    {
        if (_baseEnemy.GetIsHumaniod())
        {
            if (navMeshAgent != null)
            {
                _navMeshAgent = navMeshAgent;
            }
        }
    }

    public virtual void SetExecutingFSM(EnemyFSM fsm)
    {
        if (fsm != null)
        {
            _enemyFSM = fsm;
        }
    }

    public virtual void SetExecutingEnemy(BaseEnemy BaseEnemy)
    {
        if (BaseEnemy != null)
        {
            _baseEnemy = BaseEnemy;
            _triggerSensor = _baseEnemy.EnemyTriggerSensor;
            ET = _baseEnemy.getEnemyType();
        }
    }

    public virtual void SetExecutingAnimator(Animator AC)
    {
        if (_baseEnemy.GetIsHumaniod())
        {
            if (AC != null)
            {
                _AC = AC;
            }
        }
    }
}
