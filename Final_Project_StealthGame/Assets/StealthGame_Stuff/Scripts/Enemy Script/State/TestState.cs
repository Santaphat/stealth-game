﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//[CreateAssetMenu(fileName = "TestState", menuName = "Unity-FSM/State/TestState", order = 6)]
//public class TestState : BaseState
//{
//    PatrolMarker _guardingLocation;
//    public override void OnEnable()
//    {
//        base.OnEnable();
//        StateType = FSMStateType.TEST;
//    }
//    public override bool EnterState()
//    {
//        EnteredState = true;
//        if (base.EnterState())
//        {
//            if (_baseEnemy.PatrolPoints == null || _baseEnemy.PatrolPoints.Length == 0)
//            {
//                _guardingLocation = _baseEnemy.PatrolPoints[0];
//            }
//            _humaniodEnemy.StopMovement();
//            Debug.Log(_baseEnemy.gameObject.name + " Entering Test State");
//        }
//        else
//        {
//            Debug.Log(_baseEnemy.gameObject.name + " Cannot Enter Test State");
//            EnteredState = false;
//        }

//        return EnteredState;
//    }

//    public override bool ExitState()
//    {
//        if (base.ExitState())
//        {
//            Debug.Log(_baseEnemy.gameObject.name + " Exiting Test State");
//            return true;
//        }
//        else
//        {
//            Debug.Log(_baseEnemy.gameObject.name + " Cannot Exit Test State");
//            return false;
//        }
//    }

//    public override void UpdateState()
//    {
//        if (EnteredState)
//        {
//            base.UpdateState();
//            List<GameObject> objectDetected = _triggerSensor.GetDetected();
//            if (objectDetected.Count > 0)
//            {
//                foreach (GameObject objectSense in objectDetected)
//                {
//                    if (objectSense.tag == "Player")
//                    {
//                        BodyParts temp = objectSense.GetComponent<BodyParts>();
//                        if (temp)
//                        {
//                            if (_baseEnemy.canSeeObject(temp.chest, "Player", true))
//                            {
//                                PlayerInSight(objectSense.transform);
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }

//    protected override void PlayerInSight(Transform player)
//    {
//        base.PlayerInSight(player);
//    }
//}
