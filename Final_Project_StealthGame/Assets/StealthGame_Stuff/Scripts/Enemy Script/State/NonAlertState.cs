﻿using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using UnityEngine;


[CreateAssetMenu(fileName = "NonAlertState", menuName = "Unity-FSM/State/NonAlertState", order = 1)]
public class NonAlertState : BaseState
{ 
    //For Patrol State
    PatrolMarker[] _patrolPoints;
    bool _patrolLoop;
    bool _forward;
    bool _finishLookingAroundOnPoint = false;
    bool oldSawPlayer = false;
    bool firstTimeSawPlayer;
    TurningAction _currentTurningAction;
    TurningAction _previousTurningAction;
    bool _GuardBackIntoPos;
    float tempWaitTimer = 0;

    public override void OnEnable()
    {
        base.OnEnable();
        StateType = FSMStateType.NonAlertState;
    }
    public override bool EnterState()
    {
       
        EnteredState = true;
        if (base.EnterState())
        {
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log(_baseEnemy.name + " " + _baseEnemy.GetIsHumaniod());
                Debug.Log(_baseEnemy.name + " " + _baseEnemy.getEnemyType());
            }
            //Reset What Enemy Ai Saw/ Heard
            _baseEnemy.ObjectOfInterest = Vector3.zero;
            _baseEnemy.PlayerLastKnownPosition = Vector3.zero;
            //
            if (_baseEnemy.GetIsHumaniod())
            {
                _humaniodEnemy.HeardSomethingPosition = Vector3.zero;
                _patrolPoints = _humaniodEnemy.PatrolPoints;
                if (_humaniodEnemy.HumaniodNonAlertSubState == NoneAlertSubState.Patrol)
                {
                    _patrolLoop = _humaniodEnemy.PatrolLoop;

                    if (_patrolPoints == null || _patrolPoints.Length == 0)
                    {
                        Debug.LogError("PatrolState: Failed to grab patrol points from BaseEnemy");
                    }
                    else
                    {
                        findNextPatrolIndex();
                        _humaniodEnemy.SetDestination(_patrolPoints[_baseEnemy.CurrentPatrolIndex].transform.position);
                        EnteredState = true;
                    }
                    _humaniodEnemy.StartMovement(_moveSpeed);
                }
                else if (_humaniodEnemy.HumaniodNonAlertSubState == NoneAlertSubState.Guard)
                {
                    _baseEnemy.CurrentPatrolIndex = 0;
                    _humaniodEnemy.SetDestination(_patrolPoints[_baseEnemy.CurrentPatrolIndex].transform.position);
                    _humaniodEnemy.StartMovement(_moveSpeed);
                    _GuardBackIntoPos = false;
                    EnteredState = true;
                }
                else
                {
                    if (_baseEnemy.ShowDebug)
                    {
                        Debug.Log(_baseEnemy.gameObject.name + "Cannot Enter NonAlertState State");
                    }
                    EnteredState = false;
                }
            }
            else if(_baseEnemy.getEnemyType() == EnemyType.SecurityCamera) {
                Security_Camera_AI tempCam = (Security_Camera_AI)_baseEnemy;
                //Debug.Log(tempCam.startingTurnDirection);
                _currentTurningAction = tempCam.startingTurnDirection;
                _previousTurningAction = tempCam.startingTurnDirection;
            }
            oldSawPlayer = false;
            firstTimeSawPlayer = false;
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log(_baseEnemy.gameObject.name + " Entering NonAlertState State");
            }
        }
        else
        {
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log(_baseEnemy.gameObject.name + "Cannot Enter NonAlertState State");
            }
            EnteredState = false;
        }
        return EnteredState;
    }

    public override bool ExitState()
    {
        if (base.ExitState())
        {
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log(_baseEnemy.gameObject.name + " Exiting NonAlertState State");
            }
            return true;
        }
        else
        {
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log("Cannot Exit NonAlertState State");
            }

            return false;
        }
    }
    public override void UpdateState()
    {
        if (EnteredState)
        {
            base.UpdateState();
            List<GameObject> objectDetected = _triggerSensor.GetDetected();
            if (objectDetected.Count > 0)
            {
                foreach (GameObject objectSense in objectDetected)
                {
                    if (objectSense.tag == "Player")
                    {
                        BodyParts temp = objectSense.GetComponent<BodyParts>();
                        if (temp)
                        {
                            if (_baseEnemy.canSeeObject(temp.chest, "Player", true))
                            {
                                PlayerInSight(objectSense.transform);
                            }
                        }
                        else
                        {
                            Debug.Log("Please put bodyparts script inside player for better raycast");
                            if (_baseEnemy.canSeeObject(objectSense.transform, "Player", true))
                            {
                                PlayerInSight(objectSense.transform);
                            }
                        }
                    }
                }
            }
            if (_baseEnemy.GetIsHumaniod())
            {
                if (_humaniodEnemy.HumaniodNonAlertSubState == NoneAlertSubState.Patrol)
                {
                    if (_navMeshAgent != null)
                    {
                        if (_navMeshAgent.remainingDistance <= 0.1f)
                        {
                            _humaniodEnemy.StopMovement();
                            if (_baseEnemy.TurnOrientationToObject(_patrolPoints[_baseEnemy.CurrentPatrolIndex].transform))
                            {
                                if (_patrolPoints[_baseEnemy.CurrentPatrolIndex].LookAround && !_finishLookingAroundOnPoint)
                                {
                                    _finishLookingAroundOnPoint = LookAround();
                                }
                                else
                                {
                                    findNextPatrolIndex();
                                    _humaniodEnemy.StartMovement(_moveSpeed);
                                    _humaniodEnemy.SetDestination(_patrolPoints[_baseEnemy.CurrentPatrolIndex].transform.position);
                                    _finishLookingAroundOnPoint = false;
                                    tempWaitTimer = 0.0f;
                                }
                            }
                        }
                    }
                }
                else if (_humaniodEnemy.HumaniodNonAlertSubState == NoneAlertSubState.Guard)
                {

                    if (Vector3.Distance(_navMeshAgent.destination, _patrolPoints[_baseEnemy.CurrentPatrolIndex].transform.position) > 1.0f)
                    { 
                        _humaniodEnemy.StartMovement(_moveSpeed);
                        _humaniodEnemy.SetDestination(_patrolPoints[_baseEnemy.CurrentPatrolIndex].transform.position);
                    }
                    else if (_navMeshAgent.remainingDistance <= 0.1f && !_GuardBackIntoPos)
                    {
                        _humaniodEnemy.StopMovement();
                        if (_baseEnemy.TurnOrientationToObject(_patrolPoints[_baseEnemy.CurrentPatrolIndex].transform))
                        {
                            _GuardBackIntoPos = true;
                        }
                    }
                    if (_GuardBackIntoPos)
                    {
                        LookAround();
                    }
                }
                else
                {
                    Debug.Log(_currentTurningAction);
                    LookAround();
                }
            }
            else
            {
                if (_baseEnemy.getEnemyType() == EnemyType.SecurityCamera)
                {
                    //Debug.Log("CurrentTurning:" + _currentTurningAction);
                    //Debug.Log("PreviousTurning:" + _previousTurningAction);
                    LookAround();
                }

            }
        }
    }

    bool LookAround()
    {
        if (_baseEnemy.getEnemyType() == EnemyType.SecurityCamera)
        {
            Security_Camera_AI temp_Sec_AI = (Security_Camera_AI)_baseEnemy;
            if (_currentTurningAction == TurningAction.Left)
            {
                if (temp_Sec_AI.TurnToPos(temp_Sec_AI.LeftLookPos))
                {
                    _previousTurningAction = _currentTurningAction;
                    _currentTurningAction = TurningAction.Center;
                }
            }
            else if(_currentTurningAction == TurningAction.Right)
            {
                if (temp_Sec_AI.TurnToPos(temp_Sec_AI.RightLookPos))
                {
                    _previousTurningAction = _currentTurningAction;
                    _currentTurningAction = TurningAction.Center;
                }
            }else if(_currentTurningAction == TurningAction.Center)
            {
                if (temp_Sec_AI.TurnToPos(temp_Sec_AI.CenterLookPos))
                {
                    //Debug.Log("Finish Center");
                    TurningAction temp = _previousTurningAction;
                    _previousTurningAction = _currentTurningAction;
                    if (temp == TurningAction.Left)
                    {
                        _currentTurningAction = TurningAction.Right;
                    }
                    else
                    {
                        _currentTurningAction = TurningAction.Left;
                    }
                }
            }
        }
        else
        {
            tempWaitTimer += Time.deltaTime;
            if (tempWaitTimer > 3.0f)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        ////Look left
        //if (!_lookingLeft)
        //{

        //}
        ////Look Right
        //else if (!_lookingRight)
        //{

        //}
        //if (_lookingLeft && _lookingRight)
        //{
        //    return true;
        //}
        //else
        //{
        //    return false;
        //}

        return false;
    }

    protected override void PlayerInSight(Transform player)
    {
        base.PlayerInSight(player);
        _enemyFSM.EnterState(FSMStateType.DetectingPlayer);
    }
    private void findNextPatrolIndex()
    {
        if (_baseEnemy.CurrentPatrolIndex <= 0)
        {
            _forward = true;
            _baseEnemy.CurrentPatrolIndex = 0;
        }
        if (_patrolLoop)
        {
            _baseEnemy.CurrentPatrolIndex = (_baseEnemy.CurrentPatrolIndex + 1) % _patrolPoints.Length;
        }
        else
        {
            if (_forward)
            {
                if ((_baseEnemy.CurrentPatrolIndex + 1) % _patrolPoints.Length == 0)
                {
                    _baseEnemy.CurrentPatrolIndex = _baseEnemy.CurrentPatrolIndex - 1;
                    _forward = false;
                }
                else
                {
                    _baseEnemy.CurrentPatrolIndex = (_baseEnemy.CurrentPatrolIndex + 1);
                }
            }
            else
            {
                if (_baseEnemy.CurrentPatrolIndex <= 0)
                {
                    _baseEnemy.CurrentPatrolIndex++;
                    _forward = true;
                }
                else
                {
                    _baseEnemy.CurrentPatrolIndex = _baseEnemy.CurrentPatrolIndex - 1;
                }
            }
        }
    }
}
