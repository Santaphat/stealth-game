﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.AI;

//[CreateAssetMenu(fileName = "SuspiciousState", menuName = "Unity-FSM/State/SuspicousState", order = 3)]
//public class SuspiciousState : BaseState
//{

//    public override void OnEnable()
//    {
//        base.OnEnable();
//        StateType = FSMStateType.SUSPICIOUS;
//    }


//    public override bool EnterState()
//    {
//        EnteredState = true;
//        if (base.EnterState())
//        {
//            //if (!_baseEnemy.AlertIndicatorOn)
//            //{
//            //    _baseEnemy.TurnOnAlertIndicator();
//            //}
//            //_baseEnemy.CurrentAlertSightDistance = _baseEnemy.DistanceBeforeSuspicious;
//            _baseEnemy.StartMovement(_moveSpeed);
//            _baseEnemy.SetDestination(_baseEnemy.PlayerLastKnownPosition);
//            if (_baseEnemy.PlayerLastKnownPosition != Vector3.zero && _baseEnemy.PlayerLastKnownPosition != null)
//            {
//                _baseEnemy.SetDestination(_baseEnemy.PlayerLastKnownPosition);
//            }
//            Debug.Log(_baseEnemy.gameObject.name + " Entering Suspicious State");
//        }
//        else
//        {
//            Debug.Log(_baseEnemy.gameObject.name + " Cannot Enter Suspicious State");
//            EnteredState = false;
//        }
//        return EnteredState;
//    }

//    public override bool ExitState()
//    {
//        if (base.ExitState())
//        {
//            Debug.Log(_baseEnemy.gameObject.name + " Exiting Suspicious State");
//            return true;
//        }
//        else
//        {
//            Debug.Log(_baseEnemy.gameObject.name + " Cannot Exit Suspicious State");
//            return false;
//        }
//    }

//    public override void UpdateState()
//    {
//        if (EnteredState)
//        {
//            _baseEnemy.ObjectOfInterest = Vector3.zero;
//            if (_navMeshAgent.remainingDistance <= 0.1f)
//            {
//                _baseEnemy.StopMovement();
//                if (_baseEnemy.CurrentTimerBeforeAlertDecrease > 0)
//                {
//                    _baseEnemy.CurrentTimerBeforeAlertDecrease -= Time.deltaTime;
//                }
//                else
//                {
//                    _enemyFSM.EnterState(FSMStateType.NonAlertState);
//                }
//            }
//            List<GameObject> objectDetected = _triggerSensor.GetDetected();
//            if (objectDetected.Count > 0)
//            {
//                foreach (GameObject objectSense in objectDetected)
//                {
//                    BodyParts temp = objectSense.GetComponent<BodyParts>();
//                    if (temp)
//                    {
//                        if (_baseEnemy.canSeeObject(temp.chest, "Player", true))
//                        {
//                            PlayerInSight(objectSense.transform);
//                        }
//                    }
                
//                }

//            }

//            //throw new System.NotImplementedException();
//        }
//    }

//    protected override void PlayerInSight(Transform player)
//    {
//        _baseEnemy.CurrentTimerBeforeAlertDecrease = _baseEnemy.MaxTimerBeforeAlertDecrease;
//        base.PlayerInSight(player);
//        if (_baseEnemy.PlayerLastKnownPosition != null)
//        {
//            _baseEnemy.StartMovement(_moveSpeed);
//            _baseEnemy.SetDestination(_baseEnemy.PlayerLastKnownPosition);
//        }
//        //_baseEnemy.TurnTowards(player.transform);
//        //if (_baseEnemy.CurrentAlertSightDistance < _baseEnemy.PlayerDistance)
//        //{
//        //    //_baseEnemy.CurrentTimerBeforeAlert += Time.deltaTime * (seePlayerAlertScaleTimer * (1 - ((Vector3.Distance(player.position, _baseEnemy.transform.position)) / _baseEnemy.SightDistance)));
//        //}
//        //else
//        //{
//        //    List<GameObject> enemyDetected = _triggerSensor.GetDetectedByTag("Enemy");
//        //    if (enemyDetected.Count > 0)
//        //    {
//        //        foreach (GameObject enemy in enemyDetected)
//        //        {

//        //            if (enemy.tag == "Enemy" && _baseEnemy.gameObject != enemy)
//        //            {
//        //                BaseEnemy temp = enemy.GetComponent<BaseEnemy>();
//        //                if (temp != null)
//        //                {
//        //                    temp.OtherEnemyDetectedPlayer(_baseEnemy.PlayerLastKnownPosition);
//        //                }
//        //            }
//        //        }
//        //    }
//        //_enemyFSM.EnterState(FSMStateType.COMBAT);
//        //}
//    }
//}
