﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "DetectingPlayer", menuName = "Unity-FSM/State/DetectingPlayer", order = 1)]
public class DetectingPlayer : BaseState
{
    bool _previousUpdateSawPlayer = false;
    public override void OnEnable()
    {
        base.OnEnable();
        StateType = FSMStateType.DetectingPlayer;
    }
    public override bool EnterState()
    {
        EnteredState = true;
        if (base.EnterState())
        {
            _previousUpdateSawPlayer = false;
            if (_baseEnemy.GetIsHumaniod())
            {
                _baseEnemy.EnemySoundEffectManager.PlaySoundEffect("EnemyReactionToPlayer");
                _humaniodEnemy.StopMovement();
            }
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log(_baseEnemy.gameObject.name + " Entering DetectingPlayer State");
            }
        }
        else
        {
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log("Cannot Enter DetectingPlayer State");
            }
            EnteredState = false;
        }

        return EnteredState;
    }

    public override bool ExitState()
    {
        if (base.ExitState())
        {
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log(_baseEnemy.gameObject.name + " Exiting DetectingPlayer State");
            }
            return true;
        }
        else
        {
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log("Cannot Exit DetectingPlayer State");
            }
            return false;
        }
    }
    public override void UpdateState()
    {
        if (EnteredState)
        {
            base.UpdateState();
            bool sawPlayer = false;
            List<GameObject> objectDetected = _triggerSensor.GetDetected();
            if (objectDetected.Count > 0)
            {
                foreach (GameObject objectSense in objectDetected)
                {
                    if (objectSense.tag == "Player")
                    {
                        BodyParts temp = objectSense.GetComponent<BodyParts>();
                        if (temp)
                        {
                            if (_baseEnemy.canSeeObject(temp.chest, "Player", true))
                            {
                                sawPlayer = true;
                                PlayerInSight(objectSense.transform);
                            }
                        }
                        else
                        {
                            Debug.Log("Please put bodyparts script inside player for better raycast");
                            if (_baseEnemy.canSeeObject(objectSense.transform, "Player", true))
                            {
                                sawPlayer = true;
                                PlayerInSight(objectSense.transform);
                            }
                        }
                    }
                }
            }
            //Debug.Log(oldSawPlayer + " " + sawPlayer);
            if (_previousUpdateSawPlayer && !sawPlayer)
            {
                if (_baseEnemy.GetIsHumaniod())
                {
                    _humaniodEnemy.StartMovement(_moveSpeed);
                    //Change State into SawSomethingState
                    _enemyFSM.EnterState(FSMStateType.SawSomethingSuspiciousState);
                }
                else
                {
                    _enemyFSM.EnterState(FSMStateType.NonAlertState);
                }
            }
            _previousUpdateSawPlayer = sawPlayer;
        }
        //Debug.Log(_baseEnemy.CurrentAlertSightDistance);
    }

    protected override void PlayerInSight(Transform player)
    {
        base.PlayerInSight(player);
        _baseEnemy.TurnTowards(player.position);
        if (_baseEnemy.CurrentAlertSightDistance < _baseEnemy.PlayerDistance)
        {
            //Debug.Log("CurretnAlertSightDistance:" + _baseEnemy.CurrentAlertSightDistance);
            //Debug.Log(_currentTimerBeforeAlertDecrease);
            _baseEnemy.CurrentAlertSightDistance += Time.deltaTime * _baseEnemy.IncreasingAlertFactor;
            _baseEnemy.IncreasingAlertFactor += Time.deltaTime;
        }else if(_baseEnemy.CurrentAlertSightDistance >= _baseEnemy.PlayerDistance)
        {
            List<GameObject> objectDetected = _triggerSensor.GetDetected();
            if (objectDetected.Count > 0)
            {
                foreach (GameObject objectSense in objectDetected)
                {
                    if (objectSense.tag == "Enemy")
                    {
                        if (objectSense.GetComponent<BaseEnemy>().GetIsHumaniod())
                        {
                            objectSense.GetComponent<HumaniodEnemy>().OtherEnemyDetectedPlayer(_baseEnemy.PlayerLastKnownPosition);
                        }
                    }
                }
            }
            if (_baseEnemy.GetIsHumaniod()) {
                _baseEnemy.EnemySoundEffectManager.PlaySoundEffect("EnemySpottedPlayer");
            }
            _enemyFSM.EnterState(FSMStateType.COMBAT);
        }
    }
}
