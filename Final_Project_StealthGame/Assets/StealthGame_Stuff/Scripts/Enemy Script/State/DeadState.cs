﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "DeadState", menuName = "Unity-FSM/State/DeadState", order = 1)]
public class DeadState : BaseState
{
    public override void OnEnable()
    {
        base.OnEnable();
        StateType = FSMStateType.DeadState;
    }
    public override bool EnterState()
    {
        EnteredState = true;
        if (base.EnterState())
        {
            if (_baseEnemy.GetIsHumaniod())
            {
                _humaniodEnemy.StopMovement();
            }
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log(_baseEnemy.gameObject.name + " Entering Dead State");
            }
        }
        else
        {
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log("Cannot Enter Dead State");
            }
            EnteredState = false;
        }

        return EnteredState;
    }

    public override bool ExitState()
    {
        if (base.ExitState())
        {
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log(_baseEnemy.gameObject.name + " Exiting Dead State");
            }
            return true;
        }
        else
        {
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log("Cannot Exit Dead State");
            }
            return false;
        }
    }
    public override void UpdateState()
    {
        
    }

    protected override void PlayerInSight(Transform player)
    {
        
    }
}
