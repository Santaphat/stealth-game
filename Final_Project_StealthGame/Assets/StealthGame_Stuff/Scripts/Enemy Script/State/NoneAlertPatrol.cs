﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//[CreateAssetMenu(fileName = "NoneAlertPatrol", menuName = "Unity-FSM/State/Patrol", order = 2)]
//public class NoneAlertPatrol : BaseState
//{
//    PatrolMarker[] _patrolPoints;
//    bool _patrolLoop;
//    bool _forward;
//    public override void OnEnable()
//    {
//        base.OnEnable();
//        StateType = FSMStateType.NONEALERTPATROL;
//    }

//    public override bool EnterState()
//    {
//        if (base.EnterState())
//        {
//            Debug.Log(_baseEnemy.gameObject.name + " Entering Patrol State");

//            EnteredState = false;

//            _baseEnemy.setAlertColor(Color.white);

//            _patrolPoints = _baseEnemy.PatrolPoints;
//            _patrolLoop = _baseEnemy.PatrolLoop;

//            if (_patrolPoints == null || _patrolPoints.Length == 0)
//            {
//                Debug.LogError("PatrolState: Failed to grab patrol points from BaseEnemy");
//            }
//            else
//            {
//                findNextPatrolIndex();
//                _baseEnemy.SetDestination(_patrolPoints[_baseEnemy.CurrentPatrolIndex].transform.position);
//                EnteredState = true;
//            }
//            _baseEnemy.StartMovement(_moveSpeed);

//        }
//        else
//        {
//            Debug.Log(_baseEnemy.gameObject.name + " Cannot Enter Patrol State");
//        }
//        return EnteredState;
//    }

//    public override bool ExitState()
//    {
//        if (base.ExitState())
//        {
//            Debug.Log(_baseEnemy.gameObject.name + " Exiting Patrol State");
//            return true;
//        }
//        else
//        {
//            Debug.Log(_baseEnemy.gameObject.name + " Cannot Exit Patrol State");
//            return false;
//        }
//    }

//    public override void UpdateState()
//    {
//        if (EnteredState)
//        {
//            if (_currentTimerBeforeAlertDecrease > 0)
//            {
//                _currentTimerBeforeAlertDecrease -= Time.deltaTime;
//            }
//            else
//            {
//                if (_baseEnemy.CurrentAlertSightDistance > 0)
//                {
//                    _baseEnemy.CurrentAlertSightDistance -= Time.deltaTime;
//                }
//                else
//                {
//                    _baseEnemy.FirstSeenPlayer = false;
//                }
//            }
//            List<GameObject> objectDetected = _triggerSensor.GetDetected();
//            if (objectDetected.Count > 0)
//            {
//                foreach (GameObject objectSense in objectDetected)
//                {
//                    if (objectSense.tag == "Player")
//                    {
//                        if (_baseEnemy.canSeeObject(objectSense.transform, true))
//                        {
//                            Debug.Log(_baseEnemy.gameObject.name + " SeePlayer");
//                            PlayerInSight(objectSense.transform);
//                        }
//                    }
//                }
//            }
//            //if(canSeeObject())

//            if (_navMeshAgent != null)
//            {
//                if (_navMeshAgent.remainingDistance <= 0.1f)
//                {
//                    if (_patrolPoints[_baseEnemy.CurrentPatrolIndex].LookAround)
//                    {
//                        _enemyFSM.EnterState(FSMStateType.IDLE);
//                    }
//                    else
//                    {
//                        _baseEnemy.StartMovement(_moveSpeed);
//                        findNextPatrolIndex();
//                        _baseEnemy.SetDestination(_patrolPoints[_baseEnemy.CurrentPatrolIndex].transform.position);
//                    }
//                }
//            }
//            //Debug.Log("Inside Patrol State");
//        }
//    }
    
//    private void findNextPatrolIndex()
//    {
//        if (_baseEnemy.CurrentPatrolIndex <= 0)
//        {
//            _forward = true;
//            _baseEnemy.CurrentPatrolIndex = 0;
//        }
//        if (_patrolLoop)
//        {
//            _baseEnemy.CurrentPatrolIndex = (_baseEnemy.CurrentPatrolIndex + 1) % _patrolPoints.Length;
//        }
//        else
//        {
//            if (_forward)
//            {
//                if ((_baseEnemy.CurrentPatrolIndex + 1) % _patrolPoints.Length == 0)
//                {
//                    _baseEnemy.CurrentPatrolIndex = _baseEnemy.CurrentPatrolIndex - 1;
//                    _forward = false;
//                }
//                else
//                {
//                    _baseEnemy.CurrentPatrolIndex = (_baseEnemy.CurrentPatrolIndex + 1);
//                }
//            }
//            else
//            {
//                if (_baseEnemy.CurrentPatrolIndex <= 0)
//                {
//                    _baseEnemy.CurrentPatrolIndex++;
//                    _forward = true;
//                }
//                else
//                {
//                    _baseEnemy.CurrentPatrolIndex = _baseEnemy.CurrentPatrolIndex - 1;
//                }
//            }
//        }
//    }
//    protected override void PlayerInSight(Transform player)
//    {
//        _currentTimerBeforeAlertDecrease = _maxTimerBeforeAlertDecrease;
//        if (!_baseEnemy.FirstSeenPlayer)
//        {
//            _baseEnemy.FirstSeenPlayer = true;
//            //if (_baseEnemy.TimeMangerObject != null)
//            //{
//            //    _baseEnemy.TimeMangerObject.DoSlowMotion();
//            //}
//        }
//        if (!_baseEnemy.AlertIndicatorOn)
//        {
//            _baseEnemy.TurnOnAlertIndicator();
//            //_baseEnemy.setAlertColor(_alertBarColor);
//        }
//        if (_baseEnemy.CurrentAlertSightDistance < _baseEnemy.DistanceBeforeSuspicious)
//        {
//            //_baseEnemy.CurrentTimerBeforeAlert += Time.deltaTime * (seePlayerAlertScaleTimer * (1 - ((Vector3.Distance(player.position, _baseEnemy.transform.position)) / _baseEnemy.SightDistance)));
//        }
//        else
//        {
//            _baseEnemy.PlayerLastKnownPosition = player.position;
//            _enemyFSM.EnterState(FSMStateType.SUSPICIOUS);
//        }
//    }
//}
