﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "CombatState", menuName = "Unity-FSM/State/CombatState", order = 4)]
public class CombatState : BaseState
{
    [SerializeField]
    protected float _attackDelayTimer;
    [SerializeField]
    protected float _timeBeforeEnemySearch = 1.0f;
    protected float _currenttimeBeforeEnemySearch;
    protected float _currentAttackDelay;
    public override void OnEnable()
    {
        base.OnEnable();
        StateType = FSMStateType.COMBAT;
    }

    public override bool EnterState()
    {
        EnteredState = true;
        if (base.EnterState())
        {
            if (!_baseEnemy.AlertIndicatorOn)
            {
                _baseEnemy.TurnOnAlertIndicator();
            }
            if (_baseEnemy.GetIsHumaniod())
            {
                _humaniodEnemy.StartMovement(_moveSpeed);
            }
            _baseEnemy.CurrentAlertSightDistance = _baseEnemy.PlayerDistance;
            _baseEnemy.Alerted();
            //_baseEnemy.setAlertColor(_alertBarColor);
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log(_baseEnemy.gameObject.name + " Entering Combat State");
            }
            _currentAttackDelay = 0;
            _baseEnemy.GameManager.EnemyAlerted();
        }
        else
        {
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log(_baseEnemy.gameObject.name + " Cannot Enter Combat State");
            }
            EnteredState = false;
        }

        return EnteredState;
    }

    public override bool ExitState()
    {
        if (base.ExitState())
        {
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log(_baseEnemy.gameObject.name + " Exiting Combat State");
            }
            _baseEnemy.GameManager.EnemyUnAlerted();
            return true;
        }
        else
        {
            if (_baseEnemy.ShowDebug)
            {
                Debug.Log(_baseEnemy.gameObject.name + " Cannot Exit Combat State");
            }
            return false;
        }
    }
    public override void UpdateState()
    {
        if (EnteredState)
        {
            _baseEnemy.ObjectOfInterest = Vector3.zero;
            //If Player is furthur then shooting distance
            List<GameObject> objectDetected = _triggerSensor.GetDetected();
            bool seePlayer = false;
            if (objectDetected.Count > 0)
            {
                foreach (GameObject objectSense in objectDetected)
                {
                    if (objectSense.tag == "Player")
                    {
                        BodyParts temp = objectSense.GetComponent<BodyParts>();
                        if (temp)
                        {
                            if (_baseEnemy.canSeeObject(temp.chest, "Player", true))
                            {
                                _currenttimeBeforeEnemySearch = 0;
                                seePlayer = true;
                                PlayerInSight(objectSense.transform);
                            }
                        }
                    }
                }

            }
            if (!_baseEnemy.GetIsHumaniod()) {
                if (!seePlayer)
                {
                    if (_currenttimeBeforeEnemySearch > _timeBeforeEnemySearch)
                    {
                        _enemyFSM.EnterState(FSMStateType.SEARCHAREA);
                    }
                    _currenttimeBeforeEnemySearch += Time.deltaTime;
                }
            }
            else if (_baseEnemy.GetIsHumaniod())
            {
                if (_navMeshAgent.remainingDistance < _baseEnemy.AttackRange)
                {
                    if (!seePlayer)
                    {
                        if (_currenttimeBeforeEnemySearch > _timeBeforeEnemySearch)
                        {
                            _enemyFSM.EnterState(FSMStateType.SEARCHAREA);
                        }
                        _currenttimeBeforeEnemySearch += Time.deltaTime;
                    }
                }
                if (_baseEnemy.getEnemyType() == EnemyType.MeleeGuard)
                {
                    if (_navMeshAgent.remainingDistance <= _baseEnemy.AttackRange)
                    {
                        _humaniodEnemy.StopMovement();
                    }
                }
                else if (_baseEnemy.getEnemyType() == EnemyType.RangeGuard)
                {
                    if (_navMeshAgent.remainingDistance <= _baseEnemy.AttackRange / 2)
                    {
                        _humaniodEnemy.StopMovement();
                    }
                }
            }
            

            if (_currentAttackDelay > 0)
            {
                _currentAttackDelay -= Time.deltaTime;
            }
        }
    }

    protected override void PlayerInSight(Transform player)
    {
        _baseEnemy.TurnTowards(player.position);
        base.PlayerInSight(player);

        if (_baseEnemy.GetIsHumaniod())
        {
            if (Vector3.Distance(player.position, _baseEnemy.transform.position) < _baseEnemy.AttackRange)
            {

                if (_currentAttackDelay <= 0)
                {
                    _baseEnemy.Attacking();
                    _currentAttackDelay = _attackDelayTimer;
                }
            }
            else if (_baseEnemy.PlayerLastKnownPosition != null)
            {
                if (!_baseEnemy.GetIsAttacking)
                {

                    _humaniodEnemy.StartMovement(_moveSpeed);
                    _humaniodEnemy.SetDestination(_baseEnemy.PlayerLastKnownPosition);

                }
            }
        }else if (_baseEnemy.getEnemyType() == EnemyType.SecurityCamera)
        {
            List<GameObject> objectDetected = _triggerSensor.GetDetected();
            if (objectDetected.Count > 0)
            {
                foreach (GameObject objectSense in objectDetected)
                {
                    if (objectSense.tag == "Enemy")
                    {
                        if (objectSense.GetComponent<BaseEnemy>().GetIsHumaniod())
                        {
                            objectSense.GetComponent<HumaniodEnemy>().OtherEnemyDetectedPlayer(_baseEnemy.PlayerLastKnownPosition);
                        }
                    }
                }
            }
        }

    }
}
