﻿using SensorToolkit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum SubStateChildType
{
    PATROL,
    LOOKAROUND
}
abstract public class SubChildBaseState : ScriptableObject
{
    [SerializeField]
    protected float _moveSpeed;

    [SerializeField]
    protected float _maxTimerBeforeAlertDecrease;

    protected float _currentTimerBeforeAlertDecrease;

    [SerializeField]
    protected float seePlayerAlertScaleTimer = 1;

    public ExectuionState ExectuionState { get; protected set; }
    protected NavMeshAgent _navMeshAgent;
    protected BaseEnemy _baseEnemy;
    protected EnemyFSM _enemyFSM;
    protected FiniteStateMachine _FSM;
    protected EnemyType ET;
    protected Animator _AC;
    protected TriggerSensor _triggerSensor;


    public SubStateChildType StateType { get; protected set; }

    public bool EnteredState { get; protected set; }

    public abstract void UpdateState();

    protected abstract void PlayerInSight(Transform player);

    public virtual void OnEnable()
    {
        ExectuionState = ExectuionState.NONE;
    }
    public virtual bool EnterState()
    {
        bool successNavMesh = true;
        bool successBaseEnemy = true;
        bool successAnimator = true;
        bool successPlayer = true;
        ExectuionState = ExectuionState.ACTIVE;
        successNavMesh = (_navMeshAgent != null);
        successBaseEnemy = (_baseEnemy != null);
        successAnimator = (_AC != null);
        return successNavMesh & successBaseEnemy & successAnimator & successPlayer;
    }

    public virtual bool ExitState()
    {
        ExectuionState = ExectuionState.COMPLETED;
        return true;
    }

    public virtual void SetNavMeshAgent(NavMeshAgent navMeshAgent)
    {
        if (navMeshAgent != null)
        {
            _navMeshAgent = navMeshAgent;
        }
    }

    public virtual void SetTopFSM(EnemyFSM enemyFSM)
    {
        if(enemyFSM != null)
        {
            _enemyFSM = enemyFSM;
        }
    }

    public virtual void SetExecutingFSM(FiniteStateMachine fsm)
    {
        if (fsm != null)
        {
            _FSM = fsm;
        }
    }

    public virtual void SetExecutingEnemy(BaseEnemy BaseEnemy)
    {
        if (BaseEnemy != null)
        {
            _baseEnemy = BaseEnemy;
            _triggerSensor = _baseEnemy.EnemyTriggerSensor;
            ET = _baseEnemy.getEnemyType();
        }
    }

    public virtual void SetExecutingAnimator(Animator AC)
    {
        if (AC != null)
        {
            _AC = AC;
        }
    }





}
