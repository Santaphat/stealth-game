﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//[CreateAssetMenu(fileName = "TestingSubState", menuName = "Unity-FSM/SubState/TestingSubState", order = 1)]
//public class TestingSubState : SubChildBaseState
//{
//    public override void OnEnable()
//    {
//        base.OnEnable();
//        StateType = SubStateChildType.LOOKAROUND;
//    }
//    public override bool EnterState()
//    {
//        EnteredState = true;
//        if (base.EnterState())
//        {
//            Debug.Log(_baseEnemy.gameObject.name + " Entering Testing State");
//        }
//        else
//        {
//            Debug.Log("Cannot Enter Testing State");
//            EnteredState = false;
//        }

//        return EnteredState;
//    }

//    public override bool ExitState()
//    {
//        if (base.ExitState())
//        {
//            Debug.Log(_baseEnemy.gameObject.name + " Exiting Testing State");
//            return true;
//        }
//        else
//        {
//            Debug.Log("Cannot Exit Testing State");
//            return false;
//        }
//    }
//    public override void UpdateState()
//    {
//        if (EnteredState)
//        {

//            bool seePlayer = false;
//            List<GameObject> objectDetected = _triggerSensor.GetDetected();
//            if (objectDetected.Count > 0)
//            {
//                foreach (GameObject objectSense in objectDetected)
//                {
//                    if (objectSense.tag == "Player")
//                    {
//                        BodyParts temp = objectSense.GetComponent<BodyParts>();
//                        if (temp)
//                        {
//                            if (_baseEnemy.canSeeObject(temp.chest, "Player", true))
//                            {
//                                seePlayer = true;
//                                PlayerInSight(objectSense.transform);
//                            }
//                        }
//                    }
//                }
//            }
//            if (!seePlayer)
//            {

//                if (Quaternion.Angle(_baseEnemy.transform.rotation, _baseEnemy.PatrolPoints[0].transform.rotation) > 0.01f)
//                //if (_baseEnemy.transform.rotation.y != _baseEnemy.PatrolPoints[0].transform.rotation.y)
//                {
//                    //_baseEnemy.TurnTowards(_baseEnemy.PatrolPoints[0].transform);
//                }
//                else
//                {
//                    Debug.Log("Turn Finish");
//                }
//                if (_baseEnemy.ObjectOfInterest != Vector3.zero)
//                {
//                    _baseEnemy.ObjectOfInterest = Vector3.zero;
//                }
//            }
//        }
//    }

//    protected override void PlayerInSight(Transform player)
//    {
//        //_baseEnemy.TurnTowards(player);
//        _baseEnemy.ObjectOfInterest = player.position;
//        Debug.Log("See Player");
//        _baseEnemy.TurnOnAlertIndicator();
//        _baseEnemy.PlayerDistance = Vector3.Distance(player.transform.position, _baseEnemy.transform.position);

//        if (_baseEnemy.CurrentAlertSightDistance < _baseEnemy.PlayerDistance)
//        {
//            _baseEnemy.CurrentAlertSightDistance += Time.deltaTime;
//        }
//    }
//}
