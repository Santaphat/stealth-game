﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    [SerializeField]
    float _slowDownFactor;
    [SerializeField]
    float _slowDownLength;
    private void Start()
    {
        
    }
    private void Update()
    {
        Time.timeScale += (1.0f / _slowDownLength) * Time.unscaledDeltaTime;
        Time.timeScale = Mathf.Clamp(Time.timeScale, 0.0f, 1.0f);
    }
    public void DoSlowMotion()
    {
        Time.timeScale = _slowDownFactor;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
        Debug.Log("SlowDown");
    }

}
