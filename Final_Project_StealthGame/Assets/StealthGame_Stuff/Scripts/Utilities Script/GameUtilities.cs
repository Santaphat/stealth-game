﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UIElements;

public class GameUtilities : MonoBehaviour
{
    public static Vector3 CalcBallisticVelocityVector(Vector3 source, Vector3 target, float angle)
    {
        Vector3 direction = target - source;
        float h = direction.y;
        direction.y = 0;
        float distance = direction.magnitude;
        float a = angle * Mathf.Deg2Rad;
        direction.y = distance * Mathf.Tan(a);
        distance += h / Mathf.Tan(a);

        // calculate velocity
        float velocity = Mathf.Sqrt(distance * Physics.gravity.magnitude / Mathf.Sin(2 * a));
        return velocity * direction.normalized;
    }
    
    public static float CalculatePathLength(NavMeshAgent agent, Vector3 targetPos)
    {
        NavMeshPath path = new NavMeshPath();
        if (agent.enabled)
        {
            agent.CalculatePath(targetPos, path);
        }
        Vector3[] allWayPoints = new Vector3[path.corners.Length + 2];

        allWayPoints[0] = agent.transform.position;
        allWayPoints[allWayPoints.Length - 1] = targetPos;
        for(int i = 0; i < path.corners.Length; i++)
        {
            allWayPoints[i + 1] = path.corners[i];
        }
        float pathLength = 0.0f;
        for(int i =0; i < allWayPoints.Length-1; i++)
        {
            pathLength += Vector3.Distance(allWayPoints[i], allWayPoints[i + 1]);
        }
        return pathLength;
    }

    public static bool CanSeeObject(Transform _from, Transform _target , float _fromFov, float _fromSightDistance)
    {
        Vector3 direction = _target.position - _from.position;
        direction.y = 0;
        float angle = Vector3.Angle(direction, _from.forward);
        //Debug.Log(angle);
        if (angle < _fromFov * 0.5f)
        {
            RaycastHit hit;
            Vector3 newPos = _from.position;
            newPos.y = +0.5f;
            if (Physics.Raycast(newPos, direction, out hit, _fromSightDistance, -5, QueryTriggerInteraction.Ignore))
            {
                //Debug.Log(hit.collider.gameObject);
                if (hit.collider.tag == _from.tag)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public static Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal, Transform objectTransform)
    {
        if (!angleIsGlobal)
        {
            angleInDegrees += objectTransform.eulerAngles.y;
        }
        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }

    //public static ViewCastInfo ViewCast(float gloabalAngle, Transform objectTransform, float SightHeight, float viewDistance, LayerMask obstacleMask)
    //{
    //    Vector3 dir = DirFromAngle(gloabalAngle, true, objectTransform);
    //    RaycastHit hit;
    //    Vector3 tempObjectPos = objectTransform.position;
    //    tempObjectPos.y += SightHeight;
    //    if (Physics.Raycast(tempObjectPos, dir, out hit, viewDistance, obstacleMask)){
    //        Vector3 temp = hit.point;
    //        temp.y = objectTransform.position.y;
    //        return new ViewCastInfo(true, temp, hit.distance, gloabalAngle);
    //    }
    //    else
    //    {
    //        return new ViewCastInfo(false, objectTransform.position + dir * viewDistance, viewDistance, gloabalAngle);
    //    }
    //}

    //public static ViewTriangleData CaculateViewOfViewData(Transform baseTransform, float SightHeight, int viewAngle, float viewDistance, float meshResolution, float maskCutAwayDst, int edgeResolveIlterations, float edgeDstThreshold, LayerMask obstacleMask)
    //{
    //    int stepCount = Mathf.RoundToInt(viewAngle * meshResolution);
    //    float stepAngleSize = viewAngle / stepCount;

    //    List<Vector3> viewPoints = new List<Vector3>();
    //    ViewCastInfo oldViewCast = new ViewCastInfo();

    //    for(int i = 0; i <= stepCount; i++)
    //    {
    //        float angle = baseTransform.eulerAngles.y - viewAngle / 2 + stepAngleSize * i;
    //        ViewCastInfo newViewCast = ViewCast(angle, baseTransform, SightHeight, viewDistance, obstacleMask);
    //        if(i > 0)
    //        {
    //            bool edgeDstThresholdExceeded = Mathf.Abs(oldViewCast.dst - newViewCast.dst) > edgeDstThreshold;
    //            if (oldViewCast.hit != newViewCast.hit || (oldViewCast.hit && newViewCast.hit && edgeDstThresholdExceeded))
    //            {
    //                EdgeInfo edge = FindEdge(oldViewCast, newViewCast, edgeResolveIlterations, edgeDstThreshold, baseTransform, SightHeight, viewDistance, obstacleMask);

    //                if (edge.pointA != Vector3.zero)
    //                {
    //                    viewPoints.Add(edge.pointA);
    //                }
    //                if (edge.pointB != Vector3.zero)
    //                {
    //                    viewPoints.Add(edge.pointB);
    //                }

    //            }
    //        }
    //        viewPoints.Add(newViewCast.point);
    //        oldViewCast = newViewCast;
    //    }

    //    int vertexCount = viewPoints.Count + 1;
    //    Vector3[] vertices = new Vector3[vertexCount];
    //    int[] triangles = new int[(vertexCount - 2) * 3];

    //    vertices[0] = Vector3.zero;

    //    for(int i = 0; i < vertexCount - 1; i++)
    //    {
    //        vertices[i + 1] = baseTransform.InverseTransformPoint(viewPoints[i]) + Vector3.forward * maskCutAwayDst;

    //        if (i < vertexCount - 2)
    //        {
    //            triangles[i * 3] = 0;
    //            triangles[i * 3 + 1] = i + 1;
    //            triangles[i * 3 + 2] = i + 2;
    //        }
    //    }
    //    return new ViewTriangleData(viewPoints, vertexCount, vertices, triangles);

    //}
    //public static EdgeInfo FindEdge(ViewCastInfo minViewCast, ViewCastInfo maxViewCast, int edgeResolveIlterations, float edgeDstThreshold,  Transform objectTransform, float SightHeight, float viewDistance , LayerMask obstacleMask)
    //{
    //    float minAngle = minViewCast.angle;
    //    float maxAngle = maxViewCast.angle;

    //    Vector3 minPoint = Vector3.zero;
    //    Vector3 maxPoint = Vector3.zero;

    //    for(int i = 0; i< edgeResolveIlterations; i++)
    //    {
    //        float angle = (minAngle + maxAngle) / 2;
    //        ViewCastInfo newViewCast = ViewCast(angle, objectTransform, SightHeight, viewDistance, obstacleMask);
    //        bool edgeDstThresholdExceeded = Mathf.Abs(minViewCast.dst - newViewCast.dst) > edgeDstThreshold;
    //        if (newViewCast.hit == minViewCast.hit && !edgeDstThresholdExceeded)
    //        {
    //            minAngle = angle;
    //            minPoint = newViewCast.point;
    //        }
    //        else
    //        {
    //            maxAngle = angle;
    //            maxPoint = newViewCast.point;
    //        }
    //    }

    //    return new EdgeInfo(minPoint, maxPoint);
    //}

    //public struct ViewTriangleData
    //{
    //    public List<Vector3> viewPoints;
    //    public int vertexCount;
    //    public Vector3[] vertices;
    //    public int[] triangles;

    //    public ViewTriangleData(List<Vector3> _viewPoints, int _vertexCount, Vector3[] _vertices, int[] _triangles)
    //    {
    //        viewPoints = _viewPoints;
    //        vertexCount = _vertexCount;
    //        vertices = _vertices;
    //        triangles =  _triangles;
    //    }

    //}

    //public struct ViewCastInfo
    //{
    //    public bool hit;
    //    public Vector3 point;
    //    public float dst;
    //    public float angle;

    //    public ViewCastInfo(bool _hit, Vector3 _point, float _dst, float _angle)
    //    {
    //        hit = _hit;
    //        point = _point;
    //        dst = _dst;
    //        angle = _angle;
    //    }
    //}

    //public struct EdgeInfo
    //{
    //    public Vector3 pointA;
    //    public Vector3 pointB;

    //    public EdgeInfo(Vector3 _pointA, Vector3 _pointB)
    //    {
    //        pointA = _pointA;
    //        pointB = _pointB;
    //    }
    //}
}
