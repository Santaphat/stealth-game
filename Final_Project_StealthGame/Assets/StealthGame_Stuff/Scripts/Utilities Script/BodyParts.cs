﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyParts : MonoBehaviour
{
    public Transform head;
    public Transform chest;

    private void Start()
    {
        var childObjects = gameObject.GetComponentsInChildren<Transform>();
        foreach (var childObject in childObjects)
        {
            if (childObject.name == "Head")
            {
                head = childObject;
            }
            if(childObject.name == "Spine_03")
            {
                chest = childObject;
            }
        }
        if(head == null)
        {
            Debug.LogError("Cannot Find Head in Child Please Make Sure You Have a Model or Rename Model For Standard Name");
        }
        else if(chest == null)
        {
            Debug.LogError("Cannot Find Chest in Child Please Make Sure You Have a Model or Rename Model For Standard Name");
        }
    }
}
