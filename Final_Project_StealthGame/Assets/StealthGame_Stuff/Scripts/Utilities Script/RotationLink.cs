﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationLink : MonoBehaviour
{
    [SerializeField]
    Transform ObjectRotationTransform;
    [SerializeField]
    bool rotateX = false;
    [SerializeField]
    bool rotateY = false;
    [SerializeField]
    bool rotateZ = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Quaternion tempRotation;
        tempRotation = ObjectRotationTransform.rotation;
        if (!rotateX)
        {
            tempRotation.x = 0;
        }
        if (!rotateY)
        {
            tempRotation.y = 0;
        }
        if (!rotateZ)
        {
            tempRotation.z = 0;
        }
        transform.rotation = tempRotation;
    }
}
