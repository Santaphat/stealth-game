﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/TwoColorShader"
{
    Properties
    {
       [PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
        _FullPercent("Full Percent", Range(0.0, 1.0)) = 1
        [Header(MainColor)]_MainColor("Main Color", Color) = (0.2,1,0.2,1)
        [Header(SecondColor)]_SecondColor("Second color", Color) = (.2, .2, 1, 0)


    }

        SubShader
        {
            Tags {"Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent"}
            ZWrite Off
            Blend SrcAlpha OneMinusSrcAlpha
            Pass
            {
            CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #include "UnityCG.cginc"

                struct v2f
                {
                    float4 vertex   : SV_POSITION;
                    float2 uv        : TEXCOORD0;
                };

                fixed4 _MainColor;
                half _FullPercent;
                fixed4 _SecondColor;

                v2f vert(float4 pos : POSITION, float2 uv : TEXCOORD0)
                {
                  v2f o;
                  o.vertex = UnityObjectToClipPos(pos);
                  o.uv = uv;
                  return o;
                }

                fixed4 frag(v2f IN) : SV_Target
                {
                    float2 c = IN.uv;
                    fixed4 cout;

                    if (c.x < _FullPercent) {
                      cout = _MainColor;
                    }
                    else {
                      cout = _SecondColor;
                    }
                    return cout;

                }
            ENDCG
            }
        }
}