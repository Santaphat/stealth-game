﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextLevelCollider : MonoBehaviour
{
    [SerializeField]
    protected Level _nextLevel;
    protected GameManager _gm;

    private void Start()
    {
        _gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        if(_gm == null)
        {
            Debug.LogError("Cannot find game manger");
        }
        else
        {
            //Debug.Log(_gm.name);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            _gm.CompleteLevel(_nextLevel);
        }
    }
}
