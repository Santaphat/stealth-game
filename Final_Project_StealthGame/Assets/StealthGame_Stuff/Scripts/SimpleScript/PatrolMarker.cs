﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolMarker : MonoBehaviour
{
    [SerializeField]
    protected bool _lookAround;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawRay(this.transform.position, this.transform.forward);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, 0.4f);
    }

    public bool LookAround
    {
        get
        {
            return _lookAround;
        }
    }
}
