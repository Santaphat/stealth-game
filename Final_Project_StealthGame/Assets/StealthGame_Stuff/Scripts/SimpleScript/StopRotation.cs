﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopRotation : MonoBehaviour
{
    public Transform parent;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Update()
    {
        this.transform.rotation = Quaternion.Euler(90.0f, 0.0f, parent.gameObject.transform.rotation.z * -1.0f);
    }
}
