﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowingSoundRing : MonoBehaviour
{
    [SerializeField]
    GameObject ringEffect;
    bool _turnOn = false;
    float _timeBeforeDisapear = 0.0f;
    float _scaleOfSprite = 1.0f;
    void Update()
    {
        if (_turnOn)
        {
            if (_timeBeforeDisapear <= 0.0f)
            {
                ringEffect.SetActive(false);
                _turnOn = false;
            }
            else
            {
                _timeBeforeDisapear -= Time.deltaTime;
            }
        }
    }

    public void TurnOnSoundRing(float radius, float time)
    {
        _timeBeforeDisapear = time;
        _turnOn = true;
        ringEffect.SetActive(true);
        ringEffect.transform.localScale = new Vector3(radius/2,radius/2,radius/2);
    }
}
