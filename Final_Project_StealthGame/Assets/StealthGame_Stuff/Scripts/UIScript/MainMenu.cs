﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    protected Level _firstLevel;

   public void StartGame()
    {
        Debug.Log("Start");
        GameManager tempManger = GameObject.Find("GameManager").GetComponent<GameManager>();
        tempManger.SetNextLevel(_firstLevel);
        tempManger.NextLevel();
    }
   public void ExitGame()
    {
        Debug.Log("Exit");
        Application.Quit();
    }
}
