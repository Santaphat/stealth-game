﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class SoundEffect : Sound
{
    [SerializeField]
    public float minDist = 1;
    [SerializeField]
    public float maxDist = 10;
    [SerializeField]
    public AudioRolloffMode audioRolloffMode;
}

public class SoundEffectManager : MonoBehaviour
{
    [SerializeField]
    protected SoundEffect[] sounds;
    private void Awake()
    {
        foreach(SoundEffect s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.spatialBlend = 1;
            s.source.minDistance = s.minDist;
            s.source.maxDistance = s.maxDist;
            s.source.rolloffMode = s.audioRolloffMode;
        }
    }
    public void PlaySoundEffect(string name)
    {
        SoundEffect s = Array.Find(sounds, sounds => sounds.name == name);
        if (s == null)
        {
            Debug.Log("cannot find sound " + name);
            return;
        }
        s.source.PlayOneShot(s.clip);
    }
}
