﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

public class ItemPickUp : MonoBehaviour
{
    public Item _item;
    protected Vector3 _hiddenPosition;

    private void Awake()
    {
        _hiddenPosition = new Vector3(100f, 0f, 100f);
    }
    public void PickUp()
    {
        Debug.Log("Picking up " + _item.name);
        DestroyThisObject();
    }

    public void DelayDestory()
    {

    }

    private void DestroyThisObject()
    {
        Destroy(gameObject);
    }

    private void MoveItemToOtherPlace()
    {
        transform.position = _hiddenPosition;
    }




    #region Get/Set
    public bool IsKeyItem
    {
        get
        {
            return _item.isKeyForTheNextLevel;
        }
    }

    //public bool IsThrowableItem
    //{
    //    get
    //    {
    //        return _item.throwableItem;
    //    }
    //}

    public bool AlreadyPickUp {
        get
        {
            return _item.alreadyPickUp;
        }
        set
        {
            _item.alreadyPickUp = value;
        }
    }

    public Item GetItem{
        get { return _item; }
    }
    #endregion
}