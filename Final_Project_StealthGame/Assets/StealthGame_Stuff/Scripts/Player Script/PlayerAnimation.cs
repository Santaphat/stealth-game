﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class PlayerAnimation : MonoBehaviour
{
    PlayerProperty PP;

    private void Awake()
    {
        PP = this.GetComponent<PlayerProperty>();
    }

    public void UpdatePlayerAnimation()
    {
        if (PP.CurrentPlayerState != PlayerState.Dead)
        {
            Vector3 temp = PP.transform.rotation * new Vector3(PP.Horizontal, 0, PP.Vertical);
            PP.PlayerAnimator.SetFloat("_inputH", temp.x);
            PP.PlayerAnimator.SetFloat("_inputV", temp.z);
            PP.PlayerAnimator.SetBool("_isRunning", PP.IsRunning);
        }
        else
        {
            if (PP.PlayerAnimator.GetBool("_isAlive") == true)
            {
                PP.PlayerAnimator.SetBool("_isAlive", false);
            }
        }
        //StartCoroutine(DelayForIdleAnimation(PP.DelayTimeToChangeIdleAnimation));
    }

    IEnumerator DelayForIdleAnimation(float time)
    {
        if (PP.isCoroutineExecutingIdleAnimation)
            yield break;

        PP.isCoroutineExecutingIdleAnimation = true;

        yield return new WaitForSeconds(time);

        SwitchingIdleAnimation();
        
        PP.isCoroutineExecutingIdleAnimation = false;
    }

    public void SwitchingIdleAnimation()
    {
        //Random.seed = System.DateTime.Now.Millisecond;
        PP.IdleIndex = Random.Range(0f, 3f);
        PP.PlayerAnimator.SetFloat("_idleIndex", PP.IdleIndex);

        Debug.Log(PP.IdleIndex);

        //if (PP.IdleIndex < 1.5f)
        //{
        //    PP.PlayerAnimator.Play("Idle_01", -1, 0f);
        //}
        //else if (PP.IdleIndex >= 1.5f)
        //{
        //    PP.PlayerAnimator.Play("Idle_02", -1, 0f);
        //}
        

    }
}
