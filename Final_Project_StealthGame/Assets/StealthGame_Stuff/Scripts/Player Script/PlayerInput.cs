﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    PlayerProperty PP;
    PlayerAnimation PA;

    private void Awake()
    {
        PP = this.GetComponent<PlayerProperty>();
        PA = this.GetComponent<PlayerAnimation>();

        //projectile
        PP.LineVisual.positionCount = PP.LineSegment;

        PP.IsIdle = true;
        PP.IsWalking = false;
        PP.IsRunning = false;
    }

    private void Update()
    {
        PA.UpdatePlayerAnimation();
        if (PP.CurrentPlayerState != PlayerState.Dead && PP.CurrentPlayerState != PlayerState.InMenu)
        { 
            if (PP.CurrentPlayerState != PlayerState.Attack)
            {
                playerFaceAtCusor();
                PlayerAction();
                PlayerMovementInput();
            }
            //LaunchProjectile();
        }
    }

    private void FixedUpdate()
    {
        if (PP.CurrentPlayerState != PlayerState.Dead && PP.CurrentPlayerState != PlayerState.Attack && PP.CurrentPlayerState != PlayerState.InMenu)
        {
            if (PP.PlayerMoveVelocity != null)
                PP.PlayerRigibody.velocity = new Vector3(PP.PlayerMoveVelocity.x, PP.PlayerRigibody.velocity.y, PP.PlayerMoveVelocity.z);
        }
        else
        {
            PP.PlayerRigibody.velocity = Vector3.zero;
        }
    }

    private void PlayerMovementInput()
    {
        PP.Horizontal = Input.GetAxisRaw("Horizontal");
        PP.Vertical = Input.GetAxisRaw("Vertical");

        PP.PlayerDirection = new Vector3(PP.Horizontal, 0, PP.Vertical);
        PP.PlayerMoveVelocity = PP.PlayerDirection * PP.CurrentPlayerSpeed;
    }

    private void PlayerAction()
    {

        if (PP.PlayerRigibody.velocity != Vector3.zero)
        {
            PP.IsIdle = false;
            //Run state
            if (Input.GetKey(KeyCode.LeftShift))
            {
                PP.CurrentPlayerSpeed = PP.RunSpeed;
                PP.IsRunning = true;
                PP.IsWalking = false;
            }
            //Walk state
            else
            {
                PP.CurrentPlayerSpeed = PP.WalkSpeed;
                PP.IsRunning = false;
                PP.IsWalking = true;
            }
        }
        //idle state
        else
        {
            PP.CurrentPlayerSpeed = PP.WalkSpeed;
            PP.IsIdle = true;
            PP.IsRunning = false;
            PP.IsWalking = false;
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            GameObject _object = PP.Sensor.GetNearest();
            if (PP.checkIfObjectIsNear(_object)){
                if (_object.tag == "InteractableObject")
                    PP.InteractWithNearByObject(_object);
                else if (_object.tag == "Enemy")
                {
                    PP.triggerAttack();
                    BaseEnemy tempEnemy = _object.GetComponent<BaseEnemy>();
                    if (tempEnemy.GetIsHumaniod())
                    {
                        tempEnemy.enemyGotHit();
                    }
                }
            }
        }

        if (Input.GetMouseButton(1))
        {
            //Debug.Log("Press Q");
            if (PP.CountObtainedRock > 0 && PP.CarryRockInHand == true)
            {
                LaunchProjectile();
                //Debug.Log("Throw the rock");
            }
            else
            {
                DeactivateLineRenderer();
            }
        }
        else
        {
            DeactivateLineRenderer();
        }
        
    }

    private void LaunchProjectile()
    {
        Ray camRay = PP.PlayerCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        RaycastHit hitFront;

        if (Physics.Raycast(camRay, out hit, 200f, PP.Layer))
        {
            PP.Cursor.SetActive(true);
            if (PP.LineRenderer != null)
            {
                PP.LineRenderer.enabled = true;
            }
            //Checking if they had thrown off the point
            float tempY = hit.point.y;
            Vector3 tempHitPos = hit.point;
            tempHitPos.y = 0;
            Vector3 tempThrownPos = PP.ThrownPoint.position;
            tempThrownPos.y = 0;
            Vector3 dir = Vector3.Normalize(tempHitPos - tempThrownPos) * PP.ThrownDistance;
            if (Vector3.Distance(tempHitPos,tempThrownPos) > PP.ThrownDistance)
            {
                tempHitPos = dir + PP.ThrownPoint.position;
                tempHitPos.y = tempY;
            }
            //Checking if it hit a wall
            if(Physics.Raycast(PP.ThrownPoint.position, dir, out hitFront, PP.ThrownDistance, PP.WallLayer, QueryTriggerInteraction.Ignore))
            {
                tempHitPos = hitFront.point;
            }

            Vector3 vo = CalculateVelocty(tempHitPos, PP.ThrownPoint.position, PP.ObjCountDownTime);
            Visualize(vo, tempHitPos + Vector3.up * 0.1f);

            //transform.rotation = Quaternion.LookRotation(vo);

            if (Input.GetMouseButtonDown(0))
            {
                GameObject obj = Instantiate(PP.Projectile, PP.ThrownPoint.position, Quaternion.identity);
                obj.GetComponentInChildren<Rigidbody>().velocity = vo;
                //Debug.Log("Vo: " + vo);
                //Debug.Log("obj.velocity: " + obj.GetComponentInChildren<Rigidbody>().velocity);

                PP.CountObtainedRock--;
                PP.CarryRockInHand = false;
            }
            //Debug.Log("LaunchProjectile Active");
        }
        else
        {
            PP.Cursor.SetActive(false);
            if (PP.LineRenderer != null)
            {
                PP.LineRenderer.enabled = false;
            }
            //Debug.Log("LaunchProjectile Not active");
        }
        //Debug.Log("LaunchProjectile Complete");
    }

    private void DeactivateLineRenderer()
    {
        PP.Cursor.SetActive(false);
        if(PP.LineRenderer != null)
        {
            PP.LineRenderer.enabled = false;
        }
        Visualize(Vector3.zero, Vector3.zero);
    }

    private void Visualize(Vector3 vo, Vector3 hitpos)
    {
        PP.Cursor.transform.position = hitpos;
        for (int i = 0; i < PP.LineSegment; i++)
        {
            Vector3 pos = CalculatePosInTime(vo, i / ((float)PP.LineSegment - 1));
            PP.LineVisual.SetPosition(i, pos);
        }
    }

    Vector3 CalculateVelocty(Vector3 target, Vector3 origin, float time)
    {
        Vector3 distance = target - origin;
        Vector3 distanceXz = distance;
        distanceXz.y = 0f;

        float sY = distance.y;
        float sXz = distanceXz.magnitude;

        float Vxz = sXz * time;
        float Vy = (sY / time) + (0.5f * Mathf.Abs(Physics.gravity.y) * time);

        Vector3 result = distanceXz.normalized;
        result *= Vxz;
        result.y = Vy;

        return result;
    }

    private Vector3 CalculatePosInTime(Vector3 vo, float time)
    {
        Vector3 result = PP.ThrownPoint.position + vo * time;
        float sY = (-0.5f * Mathf.Abs(Physics.gravity.y) * (time * time)) + (vo.y * time) + PP.ThrownPoint.position.y;

        result.y = sY;

        return result;
    }

    private void playerFaceAtCusor()
    {
        PP.CameraRay = PP.PlayerCamera.ScreenPointToRay(Input.mousePosition);
        PP.GroundPlane = new Plane(Vector3.up, Vector3.zero);
        float rayLenght;

        if (PP.GroundPlane.Raycast(PP.CameraRay, out rayLenght))
        {
            Vector3 pointToLook = PP.CameraRay.GetPoint(rayLenght);
            PP.PlayerMousePos = pointToLook;
            PP.PlayerMousePosY -= this.transform.localScale.y;
            Quaternion targetRotation = Quaternion.LookRotation(pointToLook - PP.Player.position);
            targetRotation.x = 0;
            targetRotation.z = 0;
            PP.transform.rotation = Quaternion.Slerp(PP.Player.rotation, targetRotation, 7f * Time.deltaTime);
        }
    }


}
