﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController_Script : MonoBehaviour
{
    [SerializeField]
    GameObject _Door;

    protected bool _doorOpen = false;

    Animator _ac;
    // Start is called before the first frame update
    void Start()
    {
        _ac = _Door.GetComponent<Animator>();
        if(_ac == null)
        {
            Debug.LogError(gameObject.name + " does not have a door to control");
        }
    }

    public void OpenDoor()
    {
        if (!_doorOpen)
        {
            this.GetComponent<SoundEffectManager>().PlaySoundEffect("Pass");
            _ac.SetBool("Open", true);
            _doorOpen = true;
        }
        
    }
}
