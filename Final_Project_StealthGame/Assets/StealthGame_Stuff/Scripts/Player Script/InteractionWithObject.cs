﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum Interactions{
    PickupableItem,
    DoorController
}
public class InteractionWithObject : MonoBehaviour
{
    [SerializeField]
    Interactions _interaction;

    public Interactions ObjectInteractions
    {
        get
        {
            return _interaction;
        }
    }

}
