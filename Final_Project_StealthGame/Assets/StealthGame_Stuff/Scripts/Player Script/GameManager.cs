﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public enum Level{
    MatthewLevelTesting,
    SampleScene,
    TestLevelBK_Tutorial01,
    TestLevelBK_Tutorial02,
    MainMenu
}
public enum GameState
{
    InGame,
    Lose,
    Win,
    Menu
}
public class GameManager : MonoBehaviour
{
    public static GameManager GameMangerSingleton { get { return _gameManger; } }
    private static GameManager _gameManger;
    [SerializeField]
    protected GameObject _nextLevelCanvas;
    [SerializeField]
    protected GameObject _loseCanvas;
    [SerializeField]
    protected Level _firstLevel;
    [SerializeField]
    protected Level _secondLevel;

    //To change background sound
    protected int _enemyAlerted = 0;

    protected SoundManager _SM;
    
    protected Level _nextScene;

    protected GameState state;

    protected bool _playerDetected_First;
    protected bool _playerDetected_Second;
    public bool PlayerDetected{ get { return _playerDetected_First; } set { _playerDetected_First = value; } }


    private void Awake()
    {
        
        if (_gameManger != null && _gameManger != this)
        {
            Destroy(this.gameObject);
            return;
        }
        if(_gameManger == null)
        {
            _gameManger = this;
            DontDestroyOnLoad(this.gameObject);
            //Debug.Log(SceneManager.GetActiveScene().name);
            if (SceneManager.GetActiveScene().name == Level.MainMenu.ToString())
            {
                state = GameState.Menu;
            }
            else
            {
                state = GameState.InGame;
            }
            _SM = this.GetComponent<SoundManager>();
            HideCanvas();

        }
    }
    private void Start()
    {
        playOnLoadMusic();
    }
    private void Update()
    {
        if(state == GameState.InGame)
        {
            if (_playerDetected_Second != _playerDetected_First)
            {
                if (_playerDetected_First)
                {
                    _SM.PauseMusic("SilentAmbience");
                    _SM.PlayMusic("AlertedAmbience");
                }
                else
                {
                    _SM.StopMusic("AlertedAmbience");
                    _SM.PlayMusic("SilentAmbience");
                }
                _playerDetected_Second = _playerDetected_First;
            }
            if (_enemyAlerted > 0)
            {
                _playerDetected_First = true;
            }
            else
            {
                _playerDetected_First = false;
            }
        }
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            _nextScene = Level.TestLevelBK_Tutorial01;
            ResetGameManager();
            SceneManager.LoadScene(_firstLevel.ToString());
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            _nextScene = Level.TestLevelBK_Tutorial02;
            ResetGameManager();
            SceneManager.LoadScene(_secondLevel.ToString());
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            _nextScene = Level.MainMenu;
            ResetGameManager();
            SceneManager.LoadScene(Level.MainMenu.ToString());
        }
    }

    public void SetNextLevel(Level _level)
    {
        _nextScene = _level;
    }

    public void GoToMainMenu()
    {
        ResetGameManager();
        SceneManager.LoadScene(Level.MainMenu.ToString());
    }

    public void CompleteLevel(Level NextScene)
    {
        if (state == GameState.InGame)
        {
            state = GameState.Win;
            Debug.Log(_nextScene.ToString());
            if (_playerDetected_First)
            {
                _SM.StopMusic("AlertedAmbience");
                _SM.PlayMusic("SilentAmbience");
            }
            this._nextScene = NextScene;

            _nextLevelCanvas.SetActive(true);
            GameObject.Find("PlayerSetting").GetComponent<PlayerProperty>().NextPlayerState = PlayerState.InMenu;
            //Time.timeScale = 0;
            Debug.Log("Level Won");
        }
    }

    public void PlayerIsDead()
    {
        if (state == GameState.InGame)
        {
            if (_playerDetected_First)
            {
                _SM.StopMusic("AlertedAmbience");
                _SM.PlayMusic("SilentAmbience");
            }
            state = GameState.Lose;
            _loseCanvas.SetActive(true);
            GameObject.Find("PlayerSetting").GetComponent<PlayerProperty>().NextPlayerState = PlayerState.InMenu;
            Debug.Log("Game over");
        }
    }

    public void HideCanvas()
    {
        _nextLevelCanvas.SetActive(false);
        _loseCanvas.SetActive(false);
    }

    public void NextLevel()
    {
        ResetGameManager();
        SceneManager.LoadScene(_nextScene.ToString());
    }

    public void RestartCurrentLevel()
    {
        ResetGameManager();
        state = GameState.InGame;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

    }

    public void EnemyAlerted()
    {
        _enemyAlerted++;
    }

    public void EnemyUnAlerted()
    {
        _enemyAlerted--;
    }

    void ResetGameManager()
    {
        HideCanvas();
        _enemyAlerted = 0;
        if (_nextScene == Level.MainMenu)
        {
            state = GameState.Menu;
        }
        else
        {
            state = GameState.InGame;
        }
        Debug.Log(state);
        playOnLoadMusic();
    }

    void playOnLoadMusic()
    {
        stopOnUnloadMusic();
        //Debug.Log(state);
        if (state == GameState.Menu)
        {
            _SM.PlayMusic("MainMenu");
        }
        else if (state == GameState.InGame)
        {
            _SM.PlayMusic("SilentAmbience");
        }
    }

    void stopOnUnloadMusic()
    {
        _SM.StopAllMusic();
    }
}
