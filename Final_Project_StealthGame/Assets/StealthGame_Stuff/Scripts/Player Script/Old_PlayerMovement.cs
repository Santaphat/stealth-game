﻿//using SensorToolkit;

//#if UNITY_EDITOR
//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Diagnostics.Eventing.Reader;
//using System.Xml;
//using UnityEditor;
//using UnityEngine;

//public class PlayerMovement : MonoBehaviour
//{
//    public LayerMask obstacleMask;
//    [SerializeField]
//    protected int _edgeResolveIlteration = 4;
//    [SerializeField]
//    protected float _edgeDstThreshold = 0.5f;

//    private Rigidbody r = null;
//    private Animator animator = null;
//    [SerializeField]
//    private Camera playerCamera;

//    //Player Controller variables
//    [SerializeField]
//    private float currentSpeed;
//    public float walkSpeed = 5.0f;
//    public float runSpeed = 7.0f;
//    public float sneekingSpeed = 2.0f;
//    Vector3 direction;
//    Vector3 moveVelocity;
//    Vector3 mousePos;

//    private float horizontal;
//    private float vertical;
//    private bool run;
//    private bool walk;
//    private bool sneak;

//    public Sensor sensor;
//    List<GameObject> detectedEnemy;
//    [SerializeField]
//    private float checkingDistance;
//    private float checkingDistanceObj;
//    [SerializeField]
//    private float walkingNoiseRange = 5f;
//    private bool isCoroutineExecuting = false;
//    [SerializeField]
//    private float DelayTimeToDetect = 4f;

//    bool keyCardFound = false;
//    bool isDead = false;

//    private void Awake()
//    {
//        r = GetComponent<Rigidbody>();
//        animator = GetComponent<Animator>();
//        playerCamera = FindObjectOfType<Camera>();
//        run = false;
//        sneak = false;
//    }

//    private void Update()
//    {
     
//            detectedEnemy = sensor.GetDetectedByTag("Enemy");

//            horizontal = Input.GetAxisRaw("Horizontal");
//            vertical = Input.GetAxisRaw("Vertical");

//            UpdateAnimation();
//            playerInput();
//            playerFaceAtCusor();
//            StartCoroutine(DelayForSendingSound(DelayTimeToDetect));


//            direction = new Vector3(horizontal, 0, vertical);
//            moveVelocity = direction * currentSpeed;
//    }

//    private void FixedUpdate()
//    {
//        r.velocity = new Vector3(moveVelocity.x, 0, moveVelocity.z);
//    }


//    private void playerInput()
//    {
//        if (r.velocity != Vector3.zero)
//        {
//            if (Input.GetKey(KeyCode.LeftShift))
//            {
//                currentSpeed = runSpeed;
//                run = true;
//                walk = false;
//            }
//            else if (Input.GetKey(KeyCode.LeftControl))
//            {
//                currentSpeed = sneekingSpeed;
//                sneak = true;
//                walk = false;
//            }
//            else
//            {
//                currentSpeed = walkSpeed;
//                run = false;
//                sneak = false;
//                walk = true;
//            }
//        }
//        else
//        {
//            currentSpeed = walkSpeed;
//            walk = false;
//        }

//        if (Input.GetKey(KeyCode.E))
//        {
//            CollectNearByObject();
//        }
//    }

//    private void playerFaceAtCusor()
//    {
//        Ray cameraRay = playerCamera.ScreenPointToRay(Input.mousePosition);
//        Plane groundPlane = new Plane(Vector3.up, Vector3.zero);
//        float rayLenght;

//        if (groundPlane.Raycast(cameraRay, out rayLenght))
//        {
//            Vector3 pointToLook = cameraRay.GetPoint(rayLenght);
//            mousePos = pointToLook;
//            mousePos.y -= this.transform.localScale.y;
//            Quaternion targetRotation = Quaternion.LookRotation(pointToLook - transform.position);
//            targetRotation.x = 0;
//            targetRotation.z = 0;
//            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 7f * Time.deltaTime);
//        }
//    }

//    private void UpdateAnimation()
//    {
//        //playing animation
//        animator.SetFloat("inputH", horizontal);
//        animator.SetFloat("inputV", vertical);
//        animator.SetBool("IsRunning", run);
//        animator.SetBool("IsSneaking", sneak);
//    }

//    IEnumerator DelayForSendingSound(float time)
//    {
//        if (isCoroutineExecuting)
//            yield break;

//        isCoroutineExecuting = true;

//        yield return new WaitForSeconds(time);

//        SendingSound();

//        isCoroutineExecuting = false;
//    }

//    private void SendingSound()
//    {

//        List<GameObject> detectedEnemy = sensor.GetDetectedByTag("Enemy");
//        float checkingDistance;
//        foreach(GameObject obj in detectedEnemy)
//        {
//            checkingDistance = Vector3.Distance(obj.transform.position, transform.position);
//            //Debug.Log("Distance form player: " + checkingDistance + " : Obj name: " + obj.name);

//            if (walk == true && checkingDistance <= walkingNoiseRange)
//            {
//                obj.GetComponent<HumaniodEnemy>().EnemyHeardSomething(transform.position);
//            }
//            else if (run == true)
//            {
//                obj.GetComponent<HumaniodEnemy>().EnemyHeardSomething(transform.position);
//            }
//        }
//    }

//    private void CollectNearByObject()
//    {
//        List<GameObject> objects = sensor.GetDetectedByTag("KeyCard");
//        foreach (GameObject obj in objects)
//        {
//            checkingDistanceObj = Vector3.Distance(obj.transform.position, transform.position);

//            if(checkingDistanceObj <= 1.5f)
//            {
//                SetPlayerHaveKeyCard(true);
//                Destroy(obj);
//            }
//        }
//    }

//    public void playerGotHit()
//    {
//        isDead = true;
//        //GameManager.GameMangerSingleton.EndGame();
//    }

//    void SetPlayerHaveKeyCard(bool have)
//    {
//        keyCardFound = have;
//    }

//    public bool GetPlayerHaveKeyCard()
//    {
//        return keyCardFound;
//    }
//}
//#endif