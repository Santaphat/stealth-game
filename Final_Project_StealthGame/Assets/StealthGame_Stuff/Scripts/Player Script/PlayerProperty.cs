﻿using SensorToolkit;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using UnityEngine;
using UnityEngine.PlayerLoop;

public enum PlayerState
{
    Alive,
    Hidden,
    Attack,
    InMenu,
    Dead
}

public class PlayerProperty : MonoBehaviour
{
    #region Serialize Variable
    [SerializeField]
    protected Camera _playerCam;

    protected float _currentPlayerSpeed;
    [SerializeField]
    protected float _walkSpeed = 5.0f;
    [SerializeField]
    protected float _runSpeed = 7.0f;

    [SerializeField]
    protected Transform _player;
    [SerializeField]
    protected Sensor _sensor;

    //porjectile
    [SerializeField]
    protected GameObject _projectile;
    [SerializeField]
    protected GameObject _cursor;
    [SerializeField]
    protected Transform _throwPoint;
    [SerializeField]
    protected LayerMask _layer;
    [SerializeField]
    protected LayerMask _wallLayer;
    [SerializeField]
    protected LineRenderer _lineVisual;
    [SerializeField]
    protected int _lineSegment = 10;
    [SerializeField]
    protected float _objCountDownTime = 1f;
    [SerializeField]
    protected float _thrownDistance = 1.0f;

    [SerializeField]
    protected float _runningNoiseRange = 5f;
    [SerializeField]
    protected float _collectableObjectRange = 1.5f;
    [SerializeField]
    private float _delayTimeToChangeIdleAnimation = 5.0f;
    [SerializeField]
    private float _delayTimeBeforeEnemyToDetect = 4f;

    [SerializeField]
    protected Material _highlightMat;
    #endregion

    #region Unserialize Variable
    protected Rigidbody _playerRigibody = null;
    protected Animator _playerAnimator = null;
    protected ShowingSoundRing _ssr = null;

    protected Vector3 _playerDirection;
    protected Vector3 _playerMoveVelocity;
    protected Vector3 _playerMousePos;

    protected Ray _cameraRay;
    protected Plane _groundPlane;

    protected GameObject _nearGameObject;

    protected PlayerState _currentPlayerState;
    protected PlayerState _nextPlayerState;
    protected float _horizontal;
    protected float _vertical;

    protected int _countObtainedRock;

    protected bool _isRunning;
    protected bool _isWalking;
    protected bool _isIdle;
    protected bool _haveKey;
    protected bool _carryRockInHand;
    protected bool _nearObjectEnemy;

    protected float _idleIndex;
    protected bool _isCoroutineExecutingIdleAnimation = false;
    protected bool _isCoroutineExecutingSendingSound = false;

    protected LineRenderer _lr;
    #endregion

    private void Awake()
    {
        _playerRigibody = this.GetComponent<Rigidbody>();
        if (!_playerRigibody || _playerRigibody == null)
        {
            Debug.LogError("Cannot Find Rigibody");
        }

        _playerAnimator = this.GetComponentInChildren<Animator>();
        if (!_playerAnimator || _playerAnimator == null)
        {
            Debug.LogError("Cannot Find Animator");
        }
        _ssr = this.GetComponent<ShowingSoundRing>();
        if(!_ssr || _ssr == null)
        {
            Debug.LogError("Cannot Find ShowingSoundRing");
        }
        _lr = this.GetComponent<LineRenderer>();

        _sensor = this.GetComponentInChildren<Sensor>();
    }

    private void Update()
    {
        if (CurrentPlayerState != PlayerState.Dead && CurrentPlayerState != PlayerState.InMenu)
        {
            StartCoroutine(DelayForSendingSound(_delayTimeBeforeEnemyToDetect));
            if (_isRunning)
            {
                _ssr.TurnOnSoundRing(_runningNoiseRange, 0.1f);
            }
            if (_nextPlayerState != _currentPlayerState)
            {
                _currentPlayerState = _nextPlayerState;
            }

            highlightNearObject();
        }
    }
    public bool checkIfObjectIsNear(GameObject targetObject)
    {
        if (targetObject != null)
        {
            float cosAngle = Vector3.Dot(
           (targetObject.transform.position - this.transform.position).normalized,
           this.transform.forward);

            float angle = Mathf.Acos(cosAngle) * Mathf.Rad2Deg;
            //Debug.Log(angle);
            if (angle < 90)
            {
                float checkingDistance;
                Vector3 tempTargetObjPos = targetObject.transform.position;
                tempTargetObjPos.y = 0;
                Vector3 tempCurrentObjPos = transform.position;
                tempCurrentObjPos.y = 0;
                checkingDistance = Vector3.Distance(tempTargetObjPos, tempCurrentObjPos);
                if (checkingDistance <= CollectableObjectRange)
                {
                    return true;
                }
            }
        }
        return false;
    }
    void highlightNearObject()
    {
        GameObject tempObject = _sensor.GetNearest();
        if (tempObject != null && tempObject != _nearGameObject && checkIfObjectIsNear(tempObject))
        {
            addHighlightMat(tempObject);
            if (_nearGameObject != null)
            {
                removeHightlightMat(_nearGameObject);
            }
            _nearGameObject = tempObject;
        }
        else
        {
            if(_nearGameObject != null && !checkIfObjectIsNear(_nearGameObject))
            {
                removeHightlightMat(_nearGameObject);
                _nearGameObject = null;
            }
        }
    }

    void removeHightlightMat(GameObject _object)
    {
        if (_object != null)
        {
            var tempRenderer = _object.GetComponentsInChildren<Renderer>();
            foreach (var renderer in tempRenderer)
            {

                // Remove outline shaders
                var materials = renderer.sharedMaterials.ToList();

                materials.Remove(_highlightMat);


                renderer.materials = materials.ToArray();
            }
        }
    }

    void addHighlightMat(GameObject _object)
    {
        if (_object != null)
        {
            var tempRenderer = _object.GetComponentsInChildren<Renderer>();
            foreach (var renderer in tempRenderer)
            {

                // Remove outline shaders
                var materials = renderer.sharedMaterials.ToList();

                materials.Add(_highlightMat);


                renderer.materials = materials.ToArray();
            }
        }
    }
    public void afterAnimationAttackEnemy()
    {
        Debug.Log("HitEnemy");
        NextPlayerState = PlayerState.Alive;
    }

    public void triggerAttack()
    {
        PlayerAnimator.SetTrigger("_attack");
        NextPlayerState = PlayerState.Attack;
        PlayerMoveVelocity = Vector3.zero;
    }

    void OnDrawGizmos()
    {

        Gizmos.color = Color.yellow;
        float tempPos = _runningNoiseRange;
        float tempscale = 0.2f;
        Gizmos.DrawWireSphere(this.transform.position + new Vector3(tempPos, 0,0), tempscale);
        Gizmos.DrawWireSphere(this.transform.position + new Vector3(-tempPos, 0, 0), tempscale);
        Gizmos.DrawWireSphere(this.transform.position + new Vector3(0, 0, tempPos), tempscale);
        Gizmos.DrawWireSphere(this.transform.position + new Vector3(0, 0, -tempPos), tempscale);
    }

    IEnumerator DelayForSendingSound(float time)
    {
        if (_isCoroutineExecutingSendingSound)
            yield break;

        _isCoroutineExecutingSendingSound = true;

        yield return new WaitForSeconds(time);

        SendingSound();

        _isCoroutineExecutingSendingSound = false;
    }

    public void SendingSound()
    {
        List<GameObject> detectedEnemy = _sensor.GetDetectedByTag("Enemy");
        float checkingDistance;
        foreach (GameObject obj in detectedEnemy)
        {
            checkingDistance = Vector3.Distance(obj.transform.position, transform.position);
            //Debug.Log("Distance form player: " + checkingDistance + " : Obj name: " + obj.name);

            if (_isRunning == true && checkingDistance < _runningNoiseRange)
            {
                obj.GetComponent<HumaniodEnemy>().EnemyHeardSomething(transform.position);
            }
        }
    }

    public void InteractWithNearByObject(GameObject _object)
    {
        Interactions _interaction = _object.GetComponent<InteractionWithObject>().ObjectInteractions;
        if (_interaction == Interactions.PickupableItem)
        {
            ItemPickUp _objectItemPickUp = _object.GetComponent<ItemPickUp>();
            if (_objectItemPickUp == null)
            {
                Debug.Log("Can't find the ItemPickUp script inside this object name: " + _object.name);
            }
            else if (_objectItemPickUp.AlreadyPickUp == false)
            {
                if (_objectItemPickUp.GetItem.rock)
                {
                    if (!_carryRockInHand)
                    {
                        _objectItemPickUp.PickUp();
                        ObtainRock(_objectItemPickUp.GetItem);
                        Debug.Log("You have pick up a rock");
                    }
                }
                else if(_objectItemPickUp.GetItem.isKeyForTheNextLevel)
                {
                    _objectItemPickUp.PickUp();
                    ObtainKeyItem(_objectItemPickUp.GetItem);
                    Debug.Log("You have pick up a key");
                }
                else
                {
                    Debug.Log("This item has not yet included in the player");
                }
            }
        }
        else if(_interaction == Interactions.DoorController)
        {
            if (_haveKey)
            {
                _object.GetComponent<DoorController_Script>().OpenDoor();
            }
        }
    }

    public void HitPlayer()
    {
        if(_nextPlayerState != PlayerState.Dead && CurrentPlayerState != PlayerState.InMenu)
        {
            _nextPlayerState = PlayerState.Dead;
        }
    }

    public void PlayerIsDead()
    {
        Debug.Log("PlayerIsDead");
        GameObject temp = GameObject.Find("GameManager");
        if(temp != null)
        {
            temp.GetComponent<GameManager>().PlayerIsDead();
        }
        else
        {
            Debug.LogError("Cannot Find Game Manager");
        }
        
    }

    public void ObtainKeyItem(Item item)
    {
        if(item.isKeyForTheNextLevel == true)
        {
            HaveKey = true;
        }
    }

    public void ObtainRock(Item item)
    {
        if(item.rock == true)
        {
            CountObtainedRock++;
            CarryRockInHand = true;
        }
    }

    public void ResetPlayerProperty()
    {
        HaveKey = false;
        CountObtainedRock = 0;
    }

    #region get/set
    public Camera PlayerCamera
    {
        get { return _playerCam; }
    }

    public float CurrentPlayerSpeed
    {
        get { return _currentPlayerSpeed; }
        set { _currentPlayerSpeed = value; }
    }

    public float WalkSpeed
    {
        get { return _walkSpeed; }
    }

    public float RunSpeed
    {
        get { return _runSpeed; }
    }

    public Transform Player
    {
        get { return _player; }
        set { _player = value; }
    }

    public Sensor Sensor
    {
        get { return _sensor; }
    }

    public GameObject Projectile
    {
        get { return _projectile; }
        set { _projectile = value; }
    }


    public  GameObject Cursor
    {
        get { return _cursor; }
        set { _cursor = value; }
    }

    public Transform ThrownPoint
    {
        get { return _throwPoint; }
        set { _throwPoint = value; }
    }

    public LayerMask Layer
    {
        get { return _layer; }
    }
    
    public LineRenderer LineVisual
    {
        get { return _lineVisual; }
        set { _lineVisual = value; }
    }
    
    public int LineSegment
    {
        get { return _lineSegment; }
    }

    public float ObjCountDownTime
    {
        get { return _objCountDownTime; }
    }

    public float DelayTimeToChangeIdleAnimation
    {
        get { return _delayTimeToChangeIdleAnimation; }
    }

    public Rigidbody PlayerRigibody
    {
        get { return _playerRigibody; }
        set { _playerRigibody = value; }
    }

    public Animator PlayerAnimator
    {
        get { return _playerAnimator; }
        set { _playerAnimator = value; }
    }

    public Vector3 PlayerDirection
    {
        get { return _playerDirection; }
        set { _playerDirection = value; }
    }

    public Vector3 PlayerMoveVelocity
    {
        get { return _playerMoveVelocity; }
        set { _playerMoveVelocity = value; }
    }

    public Vector3 PlayerMousePos
    {
        get { return _playerMousePos; }
        set { _playerMousePos = value; }
    }

    public float PlayerMousePosX
    {
        get { return _playerMousePos.x; }
        set { _playerMousePos.x = value; }
    }

    public float PlayerMousePosY
    {
        get { return _playerMousePos.y; }
        set { _playerMousePos.y = value; }
    }

    public float PlayerMousePosZ
    {
        get { return _playerMousePos.z; }
        set { _playerMousePos.z = value; }
    }

    public Ray CameraRay
    {
        get { return _cameraRay; }
        set { _cameraRay = value; }
    }

    public Plane GroundPlane
    {
        get { return _groundPlane; }
        set { _groundPlane = value; }
    }

    public int CountObtainedRock
    {
        get { return _countObtainedRock; }
        set { _countObtainedRock = value; }
    }

    public float Horizontal
    {
        get { return _horizontal; }
        set { _horizontal = value; }
    }

    public float Vertical
    {
        get { return _vertical; }
        set { _vertical = value; }
    }

    public bool IsRunning
    {
        get { return _isRunning; }
        set { _isRunning = value; }
    }

    public bool IsWalking
    {
        get { return _isWalking; }
        set { _isWalking = value; }
    }

    public bool IsIdle
    {
        get { return _isIdle; }
        set { _isIdle = value; }
    }

    public bool HaveKey
    {
        get { return _haveKey; }
        set { _haveKey = value; }
    }

    public bool CarryRockInHand
    {
        get { return _carryRockInHand; }
        set { _carryRockInHand = value; }
    }

    public float IdleIndex
    {
        get { return _idleIndex; }
        set { _idleIndex = value; }
    }

    public bool isCoroutineExecutingIdleAnimation
    {
        get { return _isCoroutineExecutingIdleAnimation; }
        set { _isCoroutineExecutingIdleAnimation = value; }
    }

    public LineRenderer LineRenderer
    {
        get { return _lr; }
    }

    public float CollectableObjectRange
    {
        get { return _collectableObjectRange; }
    }

    public PlayerState CurrentPlayerState {
        get { return _currentPlayerState; }
    }

    public PlayerState NextPlayerState
    {
        get { return _nextPlayerState; }
        set 
        {
            if (_nextPlayerState != PlayerState.Dead) { _nextPlayerState = value; }
        }
    }

    public float ThrownDistance
    {
        get
        {
            return _thrownDistance;
        }
    }
    public LayerMask WallLayer
    {
        get
        {
            return _wallLayer;
        }
    }
    #endregion
}
