﻿using SensorToolkit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowableRock : MonoBehaviour
{
    [SerializeField]
    protected Sensor _sensor;
    [SerializeField]
    protected float _waitingTime = 3f;
    [SerializeField]
    protected float _objHitNoiseRange = 10f;
    [SerializeField]
    protected GameObject parent;

    protected SoundEffectManager _sem;

    protected bool _objectMadeSound = false;

    protected bool _isCoroutineExecutingBeforeDestroyObj = false;

    private void Awake()
    {
        _sem = this.GetComponent<SoundEffectManager>();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "GameEnvironment")
        {
            //Debug.Log(this.name + " Entered Collision");
            _sem.PlaySoundEffect("HitFloor");
            if (!_objectMadeSound)
            {
                ObjMakingSound();
                StartCoroutine(DelayBeforeDestroyObj(_waitingTime));
                _objectMadeSound = true;
            }
        }
    }


    IEnumerator DelayBeforeDestroyObj(float time)
    {
        if (_isCoroutineExecutingBeforeDestroyObj)
            yield break;

        _isCoroutineExecutingBeforeDestroyObj = true;

        yield return new WaitForSeconds(time);

        Destroy(parent);

        _isCoroutineExecutingBeforeDestroyObj = false;
    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        float tempPos = _objHitNoiseRange;
        float tempscale = 0.2f;
        Gizmos.DrawWireSphere(this.transform.position + new Vector3(tempPos, 0, 0), tempscale);
        Gizmos.DrawWireSphere(this.transform.position + new Vector3(-tempPos, 0, 0), tempscale);
        Gizmos.DrawWireSphere(this.transform.position + new Vector3(0, 0, tempPos), tempscale);
        Gizmos.DrawWireSphere(this.transform.position + new Vector3(0, 0, -tempPos), tempscale);
    }

    public void ObjMakingSound()
    {
        this.GetComponent<ShowingSoundRing>().TurnOnSoundRing(_objHitNoiseRange, _waitingTime);
        List<GameObject> detectedEnemy = _sensor.GetDetectedByTag("Enemy");
        float checkingDistance;
        foreach (GameObject obj in detectedEnemy)
        {
            checkingDistance = Vector3.Distance(obj.transform.position, transform.position);
            Debug.Log("Distance from rock: " + this.transform.position + " " + checkingDistance + " : Obj name: " + obj.name);
            obj.GetComponent<HumaniodEnemy>().EnemyHeardSomething(transform.position);

        }
    }
}
