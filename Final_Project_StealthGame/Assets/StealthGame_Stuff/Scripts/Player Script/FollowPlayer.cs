﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{

    public Transform player;
    public float smooth = 0.3f;

    public float distanceXFromPlayer;
    public float distanceYFromPlayer;
    public float distanceZFromPlayer;

    private Vector3 velocity = Vector3.zero;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 pos = new Vector3();
        pos.x = player.position.x - distanceXFromPlayer;
        pos.z = player.position.z - distanceZFromPlayer;
        pos.y = player.position.y + distanceYFromPlayer;

       //transform.position = Vector3.Lerp(transform.position, pos, smooth);
       transform.position = Vector3.SmoothDamp(transform.position, pos, ref velocity, smooth);
    }
}
