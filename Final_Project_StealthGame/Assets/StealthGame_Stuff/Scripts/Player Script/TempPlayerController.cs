﻿using SensorToolkit;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Drawing;
using UnityEngine;

public class TempPlayerController : MonoBehaviour
{

    public float playerAttackDistance = 1.0f;
    public float moveSpeed = 5.0f;
    public float bulletSpeed = 10.0f;

    [SerializeField]
    protected int _edgeResolveIlteration = 4;

    [SerializeField]
    protected float _edgeDstThreshold = 0.5f;
    public float DelayShoot = 1.0f;

    public Transform _headtransform;

    public Transform spawnPoint;
    public GameObject throwPrefab;
    public Transform itemExitPoint;
    public Collider playerCollider;
   

    public NormalGuard guard;

    private Rigidbody myRigidBody;
    private List<Collider> enemyInRange;
    private List<Collider> closeInteractionObject;

    public TriggerSensor _triggerSensor;

    GameObject hidingSpot;


    private Vector3 moveInput;
    private Vector3 moveVelocity;
    private float currentShootDelay;
    Vector3 mousePos;

    private bool hidden;
    bool carryBody;
    GameObject body;

    public Transform mainCamera;

    float Items = 2;
    float currentItem = 0;


    void Start()
    {

        myRigidBody = GetComponent<Rigidbody>();
        enemyInRange = new List<Collider>();
        closeInteractionObject = new List<Collider>();
        playerCollider = this.GetComponent<Collider>();
        currentShootDelay = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //List<GameObject> DetectedObject = _triggerSensor.GetDetected();
        //foreach(GameObject sense in DetectedObject)
        //{
        //    if(sense.tag == "Enemy")
        //    {
        //        sense.GetComponent<BaseEnemy>().ShowEnemyViewMesh();
        //    }
        //}
        //Debug.Log(interactColliderInRadius.Count);
        //if (interactColliderInRadius.Count > 0)
        //{
        //    for (int i = 0; i < interactColliderInRadius.Count; i++)
        //    {
        //        Vector3 a = this.transform.position;
        //        a.y = 0;
        //        Vector3 b = interactColliderInRadius[i].transform.position;
        //        b.y = 0;
        //        Debug.Log(i + " " + interactColliderInRadius[i] + " " + Vector3.Distance(a,b));
        //    }
        //}
        //if (Input.GetKeyDown(KeyCode.Q))
        //{
        //    guard.EnemyHeardSomething(this.transform.position);
        //}


        if (hidden)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                hidden = false;
                this.transform.position = hidingSpot.transform.position + hidingSpot.transform.forward * 1.0f;
                this.transform.rotation = hidingSpot.transform.rotation;
                playerCollider.enabled = true;
                myRigidBody.isKinematic = false;
            }
            moveVelocity = Vector3.zero;
        }
        else
        {
            moveInput = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
            moveVelocity = moveInput * moveSpeed;

            Ray cameraRay = UnityEngine.Camera.main.ScreenPointToRay(Input.mousePosition);
            Plane groundPlane = new Plane(Vector3.up, transform.position);
            float rayLength;

            if (groundPlane.Raycast(cameraRay, out rayLength))
            {
                Vector3 pointToLook = cameraRay.GetPoint(rayLength);
                mousePos = pointToLook;
                mousePos.y -= this.transform.localScale.y;
                Quaternion targetRotation = Quaternion.LookRotation(pointToLook - transform.position);
                targetRotation.x = 0;
                targetRotation.z = 0;
                transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 7f * Time.deltaTime);
            }

            //if (Input.GetKeyDown(KeyCode.E))
            //{
            //    float minRange = int.MaxValue;
            //    int enemyID = -1;
            //    for (int i = 0; i < enemyInRange.Count; i++)
            //    {
            //        Vector3 direction = enemyInRange[i].transform.position - transform.position;
            //        float angle = Vector3.Angle(direction, transform.forward);
            //        //Debug.Log(angle);
            //        if (angle < playerFieldOfView * 0.5f)
            //        {
            //            RaycastHit hit;
            //            Vector3 newPos = transform.position;
            //            newPos.y = +0.5f;
            //            if (Physics.Raycast(newPos, direction, out hit, playerAttackDistance, -5, QueryTriggerInteraction.Ignore))
            //            {
            //                //Debug.Log(hit.collider.gameObject);
            //                if (hit.collider.tag == "Enemy")
            //                {
            //                    if (hit.distance < minRange)
            //                    {
            //                        minRange = hit.distance;
            //                        enemyID = i;

            //                    }
            //                }
            //            }
            //        }
            //    }
            //    if (enemyID != -1)
            //    {
            //        enemyInRange[enemyID].GetComponent<SimpleEnemyAI>().EnemyHit();
            //        closeInteractionObject.Add(enemyInRange[enemyID]);
            //    }
            //}
            //else if (Input.GetKeyDown(KeyCode.Space))
            //{
            //    if (!carryBody)
            //    {
            //        float minRange = int.MaxValue;
            //        int interactID = -1;
            //        for (int i = 0; i < closeInteractionObject.Count; i++)
            //        {
            //            float dist = Vector3.Distance(this.transform.position, closeInteractionObject[i].transform.position);
            //            if (dist < minRange)
            //            {
            //                minRange = dist;
            //                interactID = i;
            //            }
            //        }
            //        if (interactID != -1)
            //        {

            //            if (closeInteractionObject[interactID].gameObject.tag == "HidingSpot")
            //            {
            //                hidden = true;
            //                playerCollider.enabled = false;
            //                MR.enabled = false;
            //                myRigidBody.isKinematic = true;
            //                hidingSpot = closeInteractionObject[interactID].gameObject;
            //                this.transform.position = hidingSpot.transform.position + hidingSpot.transform.forward * 1;
            //                this.transform.rotation = hidingSpot.transform.rotation;
            //            }
            //            else if (closeInteractionObject[interactID].gameObject.tag == "Radio")
            //            {
            //                RadioInteraction RI = closeInteractionObject[interactID].gameObject.GetComponent<RadioInteraction>();
            //                if (!RI.getIsOn())
            //                {
            //                    RI.TurnOn();
            //                }
            //            }
            //            else if (closeInteractionObject[interactID].gameObject.tag == "PickupAbleItem")
            //            {
            //                if (currentItem < Items)
            //                {
            //                    currentItem++;
            //                    GameObject temp = closeInteractionObject[interactID].gameObject;
            //                    closeInteractionObject.RemoveAt(interactID);
            //                    GameObject.DestroyObject(temp, 0);
            //                }

            //            }
            //            else if (closeInteractionObject[interactID].gameObject.tag == "Objective")
            //            {
            //                GameObject temp = closeInteractionObject[interactID].gameObject;
            //                closeInteractionObject.RemoveAt(interactID);
            //                GD.PlayerGrabObjective();
            //                GameObject.DestroyObject(temp, 0);
            //            }
            //            else if (closeInteractionObject[interactID].gameObject.tag == "Enemy")
            //            {

            //                closeInteractionObject[interactID].gameObject.transform.SetParent(this.gameObject.transform, true);
            //                body = closeInteractionObject[interactID].gameObject;
            //                carryBody = true;
            //            }

            //        }
            //    }
            //    else
            //    {
            //        body.transform.SetParent(null);
            //        carryBody = false;
            //    }
            //}
            //else if (Input.GetMouseButtonDown(0))
            //{
            //    if (currentItem > 0)
            //    {
            //        shootItem();
            //    }
            //}
            //else if (Input.GetMouseButtonDown(0))
            //{
            //    if (currentShootDelay <= 0)
            //    {
            //        GD.playerShot(this.transform.position);

            //        GameObject bullet = Instantiate(throwPrefab, itemExitPoint.position + itemExitPoint.forward * 1.0f, Quaternion.identity);
            //        bullet.GetComponent<Rigidbody>().AddForce(itemExitPoint.forward * bulletSpeed, ForceMode.Impulse);

            //        currentShootDelay = DelayShoot;
            //    }
            //}
        }
    }

    public void ObjectEnterRadius(Collider other)
    {
        //if (other.gameObject.tag == "Enemy")
        //{
        //    if (other.GetComponent<SimpleEnemyAI>().enemyAlive)
        //    {
        //        enemyInRange.Add(other);
        //    }
        //    else
        //    {
        //        closeInteractionObject.Add(other);
        //    }
        //}
        //else if (other.gameObject.tag == "HidingSpot" || other.gameObject.tag == "Radio" || other.gameObject.tag == "PickupAbleItem" || other.gameObject.tag == "Objective")
        //{
        //    closeInteractionObject.Add(other);
        //}
    }

    public void ObjectExitRadius(Collider other)
    {
        ////Debug.Log("Exit" + other.gameObject);
        //if (other.gameObject.tag == "Enemy")
        //{
        //    if (other.GetComponent<SimpleEnemyAI>().enemyAlive)
        //    {
        //        enemyInRange.Remove(other);
        //    }
        //    else
        //    {
        //        closeInteractionObject.Remove(other);
        //    }
        //}
        //else if (other.gameObject.tag == "HidingSpot" || other.gameObject.tag == "Radio" || other.gameObject.tag == "PickupAbleItem" || other.gameObject.tag == "Objective")
        //{
        //    closeInteractionObject.Remove(other);
        //}

    }
    private void FixedUpdate()
    {
        myRigidBody.velocity = new Vector3(moveVelocity.x, 0, moveVelocity.z);
    }

    void shootItem()
    {
        ////GD.playerShot(this.transform.position);
        //currentItem--;
        //GameObject bullet = Instantiate(throwPrefab, itemExitPoint.position + itemExitPoint.forward * 1.0f, Quaternion.identity);
        //bullet.GetComponent<Rigidbody>().velocity = GameUtilities.CalculateLaunchData(itemExitPoint.position, mousePos);
    }

    public void playerHit()
    {
        myRigidBody.velocity = Vector3.zero;
        this.transform.position = spawnPoint.position;
        if (hidden)
        {
            hidden = false;
        }
        enemyInRange.Clear();
    }
}
